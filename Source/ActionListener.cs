public class ActionListener : GlobalControl {
    
    public override void OnProcess(float delta) {
        if (Game.Control.InPlay && Game.Players.Local != null) {
            ListenPlayer(Game.Players.Local);
        }
    }

    public void ListenPlayer(Player player) {
        if (Game.Control.HasPlayerControl) {
            if (player.Avatar.StationControl.Exists) {
                if (Game.Input.Tapped(Ut.Action.Activate)) {
                    Game.UI.At<PlayerMenu>().OpenTo<StationControlMenu>();
                }
            }
        }
    }
}
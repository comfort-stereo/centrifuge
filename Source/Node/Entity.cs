using System;
using Godot;

public enum Side {
    None,
    Left,
    Right
}

public enum SyncRate {
    Constant,
    Reserved
}

public enum EntityType {
    Character,
    Static
}

public class EntityConfig {
    public string DefaultDisplayAnimation = "idle";
    public bool IsCollidable = true;
    public bool IsLockedToGround = true;
    public SyncRate SyncRate = SyncRate.Constant;
    public EntityType Type = EntityType.Character;
}

public abstract class Entity : KinematicBody2D {
    const float ReservedSyncDelay = 0.01f;
    const float MaxFallSpeed = 600;
    const float SnapToCollisionDistance = 10;

    [Install] readonly Animator Animator;
    [Install] readonly CollisionShape2D Collider;
    [Install] readonly Node2D Pivot;

    public bool IsOnDisplay => CurrentDisplayAnimation != null;

    bool isFlipped;
    public bool IsFlipped {
        get => isFlipped;
        protected set {
            if (isFlipped != value) {
                isFlipped = value;
                ScaleFlipped(isFlipped);
                if (IsMaster) {
                    Game.Network.RPCSend(this, nameof(SyncFlipped), isFlipped);
                }
                UpdateColliders();
            }
        }
    }
    
    public Vector2 Velocity { get; protected set; }
    
    public bool IsInAir => Ground == null;
    public bool IsGrounded => Ground != null;
    public bool IsTouchingRoof { get; private set; }
    public bool IsFacingWall { get; private set; }

    public bool IsMovingLeft => Velocity.IsLeft();
    public bool IsMovingRight => Velocity.IsRight();
    public bool IsMovingUp => Velocity.IsUp();
    public bool IsMovingDown => Velocity.IsDown();
    public bool IsMovingHorizontally => Velocity.HasHorizontal(); 
    public bool IsMovingVertically => Velocity.HasVertical(); 

    public Vector2 FacingDirection => IsFlipped ? Vector.Left : Vector.Right;

    public bool IsMaster => Game.Network.IsMaster(this);
    
    protected abstract EntityConfig Config { get; }

    protected bool GravityIsEnabled = true;
    protected float GravityScale => GravityIsEnabled ? 1 : 0;
    protected float Gravity => GravityScale * Constant.DefaultGravity;

    public Side FacingWallSide {
        get {
            if (IsFacingWall) {
                return IsFlipped ? Side.Left : Side.Right;
            }

            return Side.None;
        }
    }

    protected Vector2 GroundVelocity => IsGrounded ? Ground.Position - LastGroundPosition : Vector.Zero;

    float SyncDelay => Config.SyncRate == SyncRate.Constant ? 0f : ReservedSyncDelay;

    DateTime NextSyncTime;
    Node2D Ground;
    Node2D LastGround;
    Vector2 LastGroundPosition;

    string CurrentDisplayAnimation;

    protected virtual void OnAnimate(float delta, Animator animator) { }
    protected virtual void OnReady() { }
    protected virtual void OnProcess(float delta) { }
    protected virtual void OnSync(float delta, NetworkControl network) { }

    public override void _EnterTree() {
        Configure();
        ConnectNodes();
        OnReady();
    }

    public override void _PhysicsProcess(float delta) {
        Update(delta);
    }

    public void EnterDisplay(string animation = null) {
        if (animation != null && Animator.HasAnimation(animation)) {
            CurrentDisplayAnimation = animation;
        } else {
            CurrentDisplayAnimation = Config.DefaultDisplayAnimation;
        }
    }

    public void ExitDisplay() {
        CurrentDisplayAnimation = null;
    }

    public void Place(Vector2 position) {
        Enable();
        Position = position;
        Velocity = Vector.Zero; 
        Game.Task.Duration(this, 100, (progress) => Velocity = Vector.Zero);
    }

    public void Disable() {
        Position = Vector.Zero;
        Visible = false;
    }

    public void Enable() {
        Visible = true;
    }

    void Update(float delta) {
        if (IsOnDisplay) {
            if (Animator.HasAnimation(CurrentDisplayAnimation)) {
                Animator.Animate(CurrentDisplayAnimation);
            }

            Velocity = Vector.Zero;
        } else {
            if (Config.IsCollidable && Config.IsLockedToGround) {
                LockToGround();
            }

            OnProcess(delta);
            OnAnimate(delta, Animator);

            Velocity += Vector.Down * Gravity * delta;
            Velocity = Velocity.SetY(Mathf.Max(-MaxFallSpeed, Velocity.y));
            Velocity = MoveAndSlide(Velocity);
        }

        if (Math.Abs(Velocity.x) < Constant.AssumeZeroSpeed) {
            Velocity = Velocity.SetX(0);
        }

        if (Math.Abs(Velocity.y) < Constant.AssumeZeroSpeed) {
            Velocity = Velocity.SetY(0);
        }

        if (IsMaster && Config.SyncRate == SyncRate.Constant || Ut.Time > NextSyncTime) {
            Game.Network.RPCUSend(this, nameof(SyncEntity), PredictPosition(delta), Velocity);
            NextSyncTime = Ut.Time.AddSeconds(SyncDelay);
            OnSync(delta, Game.Network);
        }
    }

    void LockToGround() {
        if (IsGrounded) {
            if (Ground == LastGround) {
                MoveAndSlide(Ground.Position - LastGroundPosition);
            }

            LastGround = Ground;
            LastGroundPosition = Ground.Position;
        }
    }

    void Configure() {
        GravityIsEnabled = Config.Type == EntityType.Character;
        NextSyncTime = Ut.Time.AddSeconds(SyncDelay * Ut.Random());
        Collider.Disabled = !Config.IsCollidable;
    }

    void ConnectNodes() {
        if (GDU.Seek<Area2D>(this, "GroundDetector") is Area2D ground) {
            ground.Connect(Ut.Signal.BodyEntered, this, nameof(OnGround));
            ground.Connect(Ut.Signal.BodyExited, this, nameof(OnGroundExit));
        }

        if (GDU.Seek<Area2D>(this, "FrontDetector") is Area2D front) {
            front.Connect(Ut.Signal.BodyEntered, this, nameof(OnFront));
            front.Connect(Ut.Signal.BodyExited, this, nameof(OnFrontExit));
        }

        if (GDU.Seek<Area2D>(this, "RoofDetector") is Area2D roof) {
            roof.Connect(Ut.Signal.BodyEntered, this, nameof(OnRoof));
            roof.Connect(Ut.Signal.BodyExited, this, nameof(OnRoofExit));
        }
    }

    void ScaleFlipped(bool flipped) {
        Pivot.SetScale(flipped ? Vector.Of(-1, 1) : Vector.One);
    }

    void UpdateColliders() {
        MoveAndCollide(Vector.Zero);
    }

    [Slave]
    void SyncEntity(Vector2 position, Vector2 velocity) {
        Position = position;
        Velocity = velocity;
    }

    [Slave]
    void SyncFlipped(bool flipped) {
        IsFlipped = flipped;
    }

    Vector2 PredictPosition(float delta) {
        return Position;
    }

    protected virtual void OnGround(KinematicBody2D body) {
        Log.Verbose("Ground");
        Ground = body;
        LastGroundPosition = Ground.Position;
        MoveAndCollide(Vector.Down * SnapToCollisionDistance);
    }

    protected virtual void OnGroundExit(KinematicBody2D body) {
        Log.Verbose("Ground Exit");
        Ground = null;
    }

    protected virtual void OnRoof(KinematicBody2D body) {
        Log.Verbose("Roof");
        IsTouchingRoof = true;
    }

    protected virtual void OnRoofExit(KinematicBody2D body) {
        Log.Verbose("Roof Exit");
        IsTouchingRoof = false;
    }

    protected virtual void OnFront(KinematicBody2D body) {
        Log.Verbose("Front");
        IsFacingWall = true;
        MoveAndCollide(FacingDirection * SnapToCollisionDistance);
    }

    protected virtual void OnFrontExit(KinematicBody2D body) {
        Log.Verbose("Front Exit");
        IsFacingWall = false;
    }
}
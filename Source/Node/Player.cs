using Godot;

public class PlayerInfo {
    public readonly int ID;
    public readonly bool IsLocal;

    public readonly string Name;

    public PlayerInfo(string name, int id, bool isLocal) {
        Name = name;
        ID = id;
        IsLocal = isLocal;
    }
}

public class Player : Node {
    public PlayerInfo Info { get; private set; }
    
    public Avatar Avatar => GDU.At<Avatar>(this);

    public override void _Ready() {
        Avatar.Disable();
    }

    public void Write(PlayerInfo info) {
        Name = info.ID.ToString();
        Info = info;
    }
}
using Godot;

public class Animator : AnimationPlayer {

    public void Animate(string animation, float speed = 1) {
        SetSpeedScale(speed);
        if (CurrentAnimation != animation) {
            Play(animation);
        } 
    }

    public void AnimateAfter(string previous, string animation, float speed = 1) {
        if (CurrentAnimation != previous) {
            Animate(animation, speed);
        }
    }
}
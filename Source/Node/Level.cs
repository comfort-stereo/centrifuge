using Godot;

public class Level : Node {
    public Vector2 SpawnPosition => SpawnPoint.GetPosition();

    Position2D SpawnPoint => GDU.At<Position2D>(this, nameof(SpawnPoint));
}
public static class Constant {
    public const int MinNetworkPort = 10000;
    public const int MaxNetworkPort = 20000;

    public const int DefaultPort = MinNetworkPort;
    public const string DefaultPlayerName = "Player";
    public const string DefaultIP = "127.0.0.1";

    public const float AssumeZeroSpeed = 0.1f;
    public const float DefaultGravity = 600f;
    public const int MaxPlayerCount = 4;
    public const int BlockSize = 16;
}
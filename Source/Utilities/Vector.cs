using Godot;

public static class Vector {
    public static Vector2 Zero => Of(0, 0);
    public static Vector2 One => Of(1, 1);
    public static Vector2 Up => Of(0, -1);
    public static Vector2 Down => Of(0, 1);
    public static Vector2 Left => Of(-1, 0);
    public static Vector2 Right => Of(1, 0);

    public static Vector2 Of(float x, float y) {
        return new Vector2(x, y);
    }

    public static Vector2 SetX(this Vector2 vector, float x) {
        vector.x = x;
        return vector;
    }

    public static Vector2 SetY(this Vector2 vector, float y) {
        vector.y = y;
        return vector;
    }

    public static bool IsUp(this Vector2 vector) {
        return vector.y < 0;
    }
    
    public static bool IsDown(this Vector2 vector) {
        return vector.y > 0;
    }
    
    public static bool IsLeft(this Vector2 vector) {
        return vector.x < 0;
    }
    
    public static bool IsRight(this Vector2 vector) {
        return vector.x > 0;
    }
    
    public static bool HasHorizontal(this Vector2 vector) {
        return vector.x != 0;
    }
    
    public static bool HasVertical(this Vector2 vector) {
        return vector.y != 0;
    }
}
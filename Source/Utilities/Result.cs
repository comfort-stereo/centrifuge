using Godot;

public struct Result<T> {
    public readonly Error Error;
    public readonly T Value;

    public bool OK => Error == Error.Ok;
    public bool Failed => Error != Error.Ok;

    Result(Error error, T value) {
        Error = error;
        Value = value;
    }

    public static Result<T> Of(T value) {
        return new Result<T>(Error.Ok, value);
    }

    public static Result<T> Fail(Error error) {
        return new Result<T>(error, default(T));
    }

    public Error Unpack(out T output) {
        output = Value;
        return Error;
    }

    public Result<T> Output(out T output) {
        output = Value;
        return this;
    }
}
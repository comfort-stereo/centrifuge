using System;
using System.Collections.Generic;

public class History {
    public const int MaxSize = 1000;

    readonly List<string> Commands = new List<string>();

    int Index;

    public int Size => Commands.Count;

    public void Write(IEnumerable<string> history) {
        Clear();
        Commands.AddRange(history);
        Reset();
    }
    
    public void Clear() {
        Commands.Clear();
    }

    public void Reset() {
        Index = Commands.Count;
    }

    public string Latest() {
        if (Size == 0 || Index > Size) {
            return "";
        }

        return Commands[Commands.Count - 1];
    }

    public string Current() {
        if (Size > 0 && Index < Size) {
            return Commands[Index];
        }

        return "";
    }

    public string Previous() {
        Index = Math.Max(Index - 1, 0);
        return Current();
    }

    public string Next() {
        Index = Math.Min(Index + 1, Size);
        return Current();
    }

    public void Add(string command) {
        while (Commands.Contains(command)) {
            Commands.Remove(command);
        }

        Commands.Add(command);

        Index = Size;

        while (Size > MaxSize) {
            Commands.RemoveAt(0);
            Index = Size;
        }
    }

    public string[] Export() {
        return Commands.ToArray();
    }

    public string Complete(string text) {
        var current = text;
        while (current.Length > 0) {
            var found = Commands.FindLast((command) => command.StartsWith(current));
            if (found != null) {
                return found;
            }

            current = current.Substring(0, current.Length - 1);
        }

        return null;
    }
}
﻿public struct Option<T> {
    public T Value { get; }
    
    public bool IsSome { get; }
    public bool IsNone => !IsSome;
    public object AsObject => IsSome ? (object) Value : null;

    public static Option<T> None => new Option<T>(default(T), false);
    
    Option(T value, bool isSome) {
        Value = value;
        IsSome = isSome;
    }

    public static Option<T> Some(T value) {
        return new Option<T>(value, true);
    }

    public bool Unpack(out T value) {
        value = IsSome ? Value : default(T);
        return IsSome;
    }
}

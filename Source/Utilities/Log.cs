using System.Runtime.CompilerServices;
using Godot;

public enum LogLevel {
    Error,
    Normal,
    Verbose
}

public static class Log {
    public static bool IsEnabled = true;

    public static event Action<string> MessageLoggedEvent;
    public static event Action<string> ErrorLoggedEvent;

    public static void Verbose(object message) {
        if ((int) Game.Config.LogLevel >= (int) LogLevel.Verbose) {
            Print($"VERBOSE --- {message}");
        }
    }

    public static void Normal(object message) {
        if ((int) Game.Config.LogLevel >= (int) LogLevel.Normal) {
            Print($"LOG     --- {message}");
        }
    }

    public static void Console(object message) {
        Print($"CONSOLE --- {message}");
    }
    
    public static void Error(object message, 
                             [CallerMemberName] string caller = "", 
                             [CallerLineNumber] int line = 0, 
                             [CallerFilePath] string path = "") {
        
        Print($"ERROR   --- {message}");
        Print($"ERROR   --- Line {line} in {caller}() at {path}.");
        
        ErrorLoggedEvent.Emit(message.ToString());
    }
    
    public static void Print(object message) {
        if (IsEnabled) {
            var text = message.ToString();
            GD.Print(text);
            MessageLoggedEvent.Emit(text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Godot;

/// <summary>
/// A callback function that can be subscribed to <see cref="GDU.MissingNodeEvent"/>
/// </summary>
public delegate void FailureEventDelegate(Node node, string message);

/// <summary>
/// Exception thrown when <see cref="GDU.MissingNodeEvent"/> has no subscribers and any at-method fails to find its
/// target node. Includes the message that would normally be delivered to the event's subscribers. 
/// </summary>
public class MissingNodeException : Exception {
    public MissingNodeException(string message) : base(message) { }
}

/// <summary>
/// Exception thrown when <see cref="GDU.InstallFailedEvent"/> has no subscribers and any installed member fails to find
/// its target node. Includes the message that would normally be delivered to the event's subscribers. 
/// </summary>
public class InstallFailedException : Exception {
    public InstallFailedException(string message) : base(message) { }
}

public class InstallAttribute : Attribute {
    public readonly string Name;

    public InstallAttribute(string name = null) {
        Name = name;
    }
}

/// <summary>
/// Callback that performs an operation on each node during a tree traversal. Use of this action type in a traversal
/// will cause all nodes in the tree to be traversed like a <see cref="DescentAction"/> that always returns
/// <see cref="Descent.Continue"/>.
/// </summary>
public delegate void TraverseAction(Node node);

/// <summary>
/// Callback that performs an operation on each node during a tree traversal. Returns an enum specifying what to do
/// with the current node's children. 
/// </summary>
public delegate Descent DescentAction(Node node);

/// <summary>
/// Signals how the traversal should proceed after processing the current node. 
/// </summary>
public enum Descent {
    
    /// <summary>
    /// Signals that the traversal should continue to descend into the current node's children if there are any.
    /// </summary>
    Continue, 
    
    /// <summary>
    /// Signals that the traversal should NOT descend into the current node's children.  
    /// </summary>
    Ignore,   
    
    /// <summary>
    /// Signals that the traversal should stop immediately, traversing absolutely no additional nodes. 
    /// </summary>
    Exit      
}

public static class GDU {
    
    /// <summary>
    /// Event emitted when any at-method fails to find its target node. If this event has no subscribers and a
    /// at-method fails a <see cref="MissingNodeException"/> will be thrown instead. No exception will be thrown if
    /// there is at least one subscriber. Subscribers to this event will be given the node passed into the failed
    /// at-method and an error message string describing what went wrong.
    /// </summary>
    public static event FailureEventDelegate MissingNodeEvent;
    
    /// <summary>
    /// Event emitted when an install operation fails to find a target node. If this event has no subscribers and an
    /// install fails a <see cref="InstallFailedException"/> will be thrown instead. No exception will be thrown if
    /// there is at least one subscriber. Subscribers to this event will be given the node on which the install failed
    /// and an error message string describing what went wrong.
    /// </summary>
    public static event FailureEventDelegate InstallFailedEvent;
    
    /// <summary>
    /// Internal queue used for breadth-first search. Should be locked before use.
    /// </summary>
    static readonly Queue<Node> BFSQueue = new Queue<Node>();
    
    /// <summary>
    /// Returns the child at a given path down from the provided node.
    /// 
    /// If this method fails to find its target node, the <see cref="MissingNodeEvent"/> will be emitted if it has any
    /// subscribers. If the event has no subscribers, a <see cref="MissingNodeException"/> will be thrown instead.
    /// </summary>
    public static T AtPath<T>(this Node node, string path) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }

        var found = node.GetNode(path);
        if (found == null) {
            var message = $"GDU: Attempted to get child of [{node.Name}] at path [{path}] but it did not exist.";
            AssertMissingNode(node, message);
            return null;
        } 
        
        if (!(found is T)) {
            var message = $"GDU: Attempted to get child of [{node.Name}] at path [{path}] as invalid type " +
                          $"[{typeof(T).FullName}], found [{typeof(T).FullName}].";
            AssertMissingNode(node, message);
            return null;
        }

        return (T) found;
    }

    /// <summary>
    /// Returns the child at a given path down from the provided node. Returns null if the node is not found or is
    /// not a compatible type.
    /// </summary>
    public static T SeekPath<T>(this Node node, string path) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }
        
        return node.GetNode(path) as T;
    }

    /// <summary>
    /// Equivalent to <see cref="Seek{T}"/> but complains when a node is not found.
    /// 
    /// If this method fails to find its target node, the <see cref="MissingNodeEvent"/> will be emitted if it has any
    /// subscribers. If the event has no subscribers, a <see cref="MissingNodeException"/> will be thrown instead.
    /// </summary>
    public static T At<T>(this Node node, string name = null, Predicate<T> predicate = null) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }
        
        var child = Seek(node, name, predicate);
        if (child == null) {
            var message = name == null
                ? $"GDU: Failed to deeply get child of type [{typeof(T).FullName}] from [{node.Name}]."
                : $"GDU: Failed to deeply get child [{name}] of type [{typeof(T).FullName}] from [{node.Name}].";
            
            AssertMissingNode(node, message);
        }

        return child;
    }

    /// <summary>
    /// Recursively search a node's tree to find a child of a given type, optionally with a given name or matching a
    /// predicate. Returns null if the node is not found.
    /// </summary>
    public static T Seek<T>(this Node node, string name = null, Predicate<T> predicate = null) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }
        
        lock (BFSQueue) {
            BFSQueue.Enqueue(node);
            
            while (BFSQueue.Count > 0) {
                var current = BFSQueue.Dequeue();
                var found = SeekLocal(current, name, predicate);
                if (found != null) {
                    BFSQueue.Clear();
                    return found;
                }

                var count = current.GetChildCount();
                for (var i = 0; i < count; i++) {
                    BFSQueue.Enqueue(current.GetChild(i));
                }
            }

            return null;
        }
    }
    
    /// <summary>
    /// Equivalent to <see cref="SeekLocal{T}"/> but complains when a node is not found.
    /// 
    /// If this method fails to find its target node, the <see cref="MissingNodeEvent"/> will be emitted if it has any
    /// subscribers. If the event has no subscribers, a <see cref="MissingNodeException"/> will be thrown instead.
    /// </summary>
    public static T AtLocal<T>(this Node node, string name = null, Predicate<T> predicate = null) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }
        
        var child = SeekLocal(node, name, predicate);
        if (child == null) {
            var message = name == null
                ? $"GDU: Failed to get local child of type [{typeof(T).FullName}] from [{node.Name}]."
                : $"GDU: Failed to get local child [{name}] of type [{typeof(T).FullName}] from [{node.Name}].";
            
            AssertMissingNode(node, message);
        }

        return child;
    }

    /// <summary>
    /// Search a node's immediate children to find a child of a given type, optionally with a given name or matching a
    /// predicate. Returns null if the node is not found.
    /// </summary>
    public static T SeekLocal<T>(this Node node, string name = null, Predicate<T> predicate = null) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }
        
        if (name == null) {
            var count = node.GetChildCount();
            for (var i = 0; i < count; i++) {
                var child = node.GetChild(i);
                if (child is T casted) {
                    if (predicate == null || predicate(casted)) {
                        return casted;
                    }
                }
            }
        } else {
            var child = node.FindNode(name);
            if (child is T casted) {
                if (predicate == null || predicate(casted)) {
                    return casted;
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Recursively search a node's tree to find all children of a given type. Can filter nodes by predicate.
    /// </summary>
    public static IEnumerable<T> SeekAll<T>(this Node node, Predicate<T> predicate = null, 
                                            List<T> output = null) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }
        
        output = output ?? new List<T>();
        output.Clear();

        var count = node.GetChildCount();
        for (var i = 0; i < count; i++) {
            TraverseDFS(node.GetChild(i), (current) => {
                if (current is T casted && (predicate == null || predicate(casted))) {
                    output.Add(casted);
                }
            });
        }

        return output;
    }
    
    /// <summary>
    /// Search a node's immediate children to find all children of a given type. Can filter nodes by predicate.
    /// </summary>
    public static IEnumerable<T> SeekAllLocal<T>(this Node node, Predicate<T> predicate = null, 
                                            List<T> output = null) where T : Node {
        if (node == null) {
            throw new ArgumentNullException();
        }
        
        output = output ?? new List<T>();
        output.Clear();

        var count = node.GetChildCount();
        for (var i = 0; i < count; i++) {
            if (node.GetChild(i) is T casted && (predicate == null || predicate(casted))) {
                output.Add(casted);
            }
        }

        return output;
    }
    
    /// <summary>
    /// Equivalent to <see cref="SeekParent{T}"/> but complains when a node is not found.
    /// 
    /// If this method fails to find its target node, the <see cref="MissingNodeEvent"/> will be emitted if it has any
    /// subscribers. If the event has no subscribers, a <see cref="MissingNodeException"/> will be thrown instead.
    /// </summary>
    public static T AtParent<T>(this Node node, string name = null, Predicate<T> predicate = null) where T : Node {
        if (SeekParent(node, name, predicate) is T parent) {
            return parent;
        }
        
        var message = name == null 
            ? $"GDU: Failed to get parent of type [{typeof(T).FullName}] from [{node.Name}]."
            : $"GDU: Failed to get parent [{name}] of type [{typeof(T).FullName}] from [{node.Name}].";
        AssertMissingNode(node, message);
        return null;
    }
    
    /// <summary>
    /// Search through a node's parents to find a parent of a given type, optionally with a given name or matching a
    /// predicate. Searches up to and includes the scene tree root. Returns null if the parent is not found.
    /// </summary>
    public static T SeekParent<T>(this Node node, string name = null, Predicate<T> predicate = null) where T : Node {
        var current = node.GetParent();
        while (current != null) {
            if (current is T casted) {
                if (name == null || casted.Name == name) {
                    if (predicate == null || predicate(casted)) {
                        return casted;
                    }
                }
            }
            current = node.GetParent();
        }

        return null;
    }
    
    /// <summary>
    /// Traverse the node tree in depth-first order, applying a callback function to each node on the way down
    /// and/or on the way back up.
    /// </summary>
    public static void TraverseDFS(Node node, TraverseAction onDescent = null, TraverseAction onAscent = null) {
        if (node == null) {
            return;
        }

        if (onDescent == null) {
            TraverseDFSAuxilary(node, null, onAscent);
            return;
        } 
        
        Descent Continue(Node value) {
            onDescent.Invoke(value);
            return Descent.Continue;
        }
        
        TraverseDFSAuxilary(node, Continue, onAscent);
    }
    
    /// <summary>
    /// Traverse the node tree in depth-first order, applying a callback function to each node on the way down
    /// and/or on the way back up. See <see cref="Descent"/> to understand what the enum returned by the descending
    /// callback does.
    /// </summary>
    public static void TraverseDFS(Node node, DescentAction onDescent = null, TraverseAction onAscent = null) {
        if (node == null) {
            return;
        }
        
        TraverseDFSAuxilary(node, onDescent, onAscent);
    }

    /// <summary>
    /// Traverse the node tree in breadth-first order, applying a callback function to each node on the way down. 
    /// </summary>
    public static void TraverseBFS(Node node, TraverseAction onDescent) {
        if (node == null) {
            return;
        }
        
        Descent Continue(Node value) {
            onDescent.Invoke(value);
            return Descent.Continue;
        }
        
        TraverseBFS(node, Continue);
    }

    /// <summary>
    /// Traverse the node tree in breadth-first order, applying a callback function to each node on the way down. 
    /// </summary>
    public static void TraverseBFS(Node node, DescentAction onDescent) {
        if (node == null) {
            return;
        }
        
        TraverseBFSAuxilary(node, onDescent);
    }
    
    static Descent TraverseDFSAuxilary(Node node, DescentAction onDescent, TraverseAction onAscent) {
        var option = onDescent?.Invoke(node) ?? Descent.Continue;
        if (option != Descent.Continue) {
            return option;
        }
        
        var count = node.GetChildCount();
        for (var i = 0; i < count; i++) {
            var suboption = TraverseDFSAuxilary(node.GetChild(i), onDescent, onAscent);
            if (suboption == Descent.Exit) {
                return Descent.Exit;
            }
        }
        
        onAscent?.Invoke(node);
        
        return Descent.Continue;
    }

    static void TraverseBFSAuxilary(Node node, DescentAction onDescent) {
        lock (BFSQueue) {
            BFSQueue.Enqueue(node);

            while (BFSQueue.Count > 0) {
                var current = BFSQueue.Dequeue();

                var option = onDescent(current);
                if (option == Descent.Exit) {
                    BFSQueue.Clear();
                    return;
                }

                if (option == Descent.Continue) {
                    var count = current.GetChildCount();
                    for (var i = 0; i < count; i++) {
                        BFSQueue.Enqueue(current.GetChild(i));
                    }
                }
            }
        }
    }
    
    static void AssertMissingNode(Node node, string message) {
        if (MissingNodeEvent == null) {
            throw new MissingNodeException(message);
        }

        MissingNodeEvent(node, message);
    }
    
    static void AssertInstallFailed(Node node, string message) {
        if (InstallFailedEvent == null) {
            throw new InstallFailedException(message);
        }

        InstallFailedEvent(node, message);
    }

    /// <summary>
    /// Flags used to retrieve attributed members by reflection.
    /// </summary>
    static readonly BindingFlags BindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

    /// <summary>
    /// Set all fields or properties on the node marked with the <see cref="Install"/> attribute with the child node
    /// they reference.
    /// </summary>
    public static void Install(Node node) {
        foreach (var member in GetAttributed<InstallAttribute>(node)) {
            var type = GetMemberType(member);
            var attribute = GetMemberAttribute<InstallAttribute>(member);
            var child = GetInstalledNode(node, type, attribute.Name ?? member.Name);
            if (child == null && attribute.Name == null) {
                child = GetInstalledNode(node, type);
            }

            if (child == null) {
                var message = attribute.Name == null
                    ? $"GDU: Node [{node.Name}] does not have any installed child of type [{type.Name}]."
                    : $"GDU: Node [{node.Name}] does not have any installed child of type [{type.Name}] node with " +
                      $"name [{attribute.Name}].";
                AssertInstallFailed(node, message);
            }

            if (!SetMemberValue(node, member, child)) {
                var message = $"GDU: Failed to set installed member [{member.Name}] of node [{node.Name}]. Must be a" +
                              "settable property or field.";
                AssertInstallFailed(node, message);
            }
        }
    }

    public static void InstallEverywhere(Node node) {
        TraverseBFS(node, (current) => Install(current));
    }
    
    public static bool IsBaseGodotType(Type type) {
        return type.Assembly.FullName.StartsWith("GodotSharp,");
    }
    
    public static bool IsBaseGodotNode(Node node) {
        return IsBaseGodotType(node.GetType());
    }
    
    static IEnumerable<Type> TypeChain(object obj) {
        var current = obj.GetType();
        while (current != null && !IsBaseGodotType(current)) {
            yield return current;
            current = current.BaseType;
        }
    }

    static Type GetMemberType(MemberInfo member) {
        if (member is PropertyInfo property) {
            return property.PropertyType;
        }

        if (member is FieldInfo field) {
            return field.FieldType;
        }

        return null;
    }

    static T GetMemberAttribute<T>(MemberInfo member) where T : Attribute {
        return Attribute.GetCustomAttribute(member, typeof(T)) as T;
    }

    static bool SetMemberValue(object obj, MemberInfo member, object value) {
        if (member is PropertyInfo property) {
            try {
                property.SetValue(obj, value);
            } catch {
                return false;
            }
        }

        if (member is FieldInfo field) {
            field.SetValue(obj, value);
        }

        return true;
    }

    static IEnumerable<MemberInfo> GetFieldsAndProperties(object obj) {
        var members = new HashSet<MemberInfo>();
        var types = TypeChain(obj);
        foreach (var type in types) {
            foreach (var property in type.GetProperties(BindingFlags)) {
                members.Add(property);
            }

            foreach (var field in type.GetFields(BindingFlags)) {
                members.Add(field);
            }
        }

        return members;
    }

    static Dictionary<Tuple<Type, Type>, MemberInfo[]> AttributedMemberCache = 
        new Dictionary<Tuple<Type, Type>, MemberInfo[]>();
    
    static IEnumerable<MemberInfo> GetAttributed<T>(object obj) where T : Attribute {
        var type = obj.GetType();
        var key = new Tuple<Type, Type>(type, typeof(T));
        var cached = AttributedMemberCache.GetOrDefault(key); 
        if (cached != null) {
            return cached;
        }
        var members = GetFieldsAndProperties(obj).Where((member) => GetMemberAttribute<T>(member) != null).ToArray();
        AttributedMemberCache.Add(key, members);
        return members;
    }

    static Node GetInstalledNode(Node node, Type type, string name = null) {
        Node target = null;
        
        bool IsCorrectType(Node child) => child.GetType().IsAssignableFrom(type);
        TraverseBFS(node, (current) => {
            target = SeekLocal<Node>(current, name, IsCorrectType);
            return target == null ? Descent.Continue : Descent.Exit; 
        });

        return target;
    }
}


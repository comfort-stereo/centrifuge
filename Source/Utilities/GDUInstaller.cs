﻿using Godot;

public class GDUInstaller : Node {
    public override void _EnterTree() {
        GetTree().Connect("node_added", this, nameof(OnNodeAdded));
    }

    void OnNodeAdded(Node node) {
        GDU.InstallEverywhere(node);
    }
}

using Godot;

public class PixelSprite : Sprite {
    const float CorrectionSpeed = 8f;

    Vector2 OriginalPosition;

    public override void _EnterTree() {
        OriginalPosition = GetPosition();
    }

    public override void _Process(float delta) {
        Snap(delta);
    }

    public void Snap(float delta) {
        Position = OriginalPosition;
        var destination = GlobalPosition.Snapped(Vector.One);
        GlobalPosition = GlobalPosition.LinearInterpolate(destination, CorrectionSpeed * delta);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using Godot;

public delegate bool Predicate<in T>(T element);

public delegate bool Predicate();

public delegate O Transform<in T, out O>(T element);

public delegate O Transform<in A, in B, out O>(A first, B second);

public delegate O Transform<in A, in B, in C, out O>(A first, B second, C third);

public delegate void Action<in T>(T element);

public delegate void Action<in A, in B>(A first, B second);

public delegate void Action<in A, in B, in C>(A first, B second, C third);

public delegate void Callback();

public delegate T Callback<out T>();

public static class Ut {
    static readonly Random Randomizer = new Random();

    public static float RotationSnap(float degrees, float size) {
        return Mathf.Round(degrees / size) * size;
    }

    public static float Random() {
        return (float) Randomizer.NextDouble();
    }

    public static int RandomInt(int max) {
        return Randomizer.Next(max);
    }

    public static float RandomInt(int min, int max) {
        return Randomizer.Next(min, max);
    }

    public static DateTime Time => DateTime.Now;

    public static void Emit(this Callback callback) {
        if (callback != null) {
            callback();
        }
    }

    public static void Emit<T>(this Action<T> action, T argument) {
        if (action != null) {
            action(argument);
        }
    }

    public static void Emit<A, B>(this Action<A, B> action, A first, B second) {
        if (action != null) {
            action(first, second);
        }
    }

    public static string BooleanToString(bool boolean) {
        return boolean ? "true" : "false";
    }

    public static Option<bool> ParseBoolean(string text) {
        if (text == "true") {
            return Option<bool>.Some(true);
        }

        if (text == "false") {
            return Option<bool>.Some(false);
        }

        return Option<bool>.None;
    }

    public static Option<int> ParseInteger(string text) {
        try {
            return Option<int>.Some(Convert.ToInt32(text));
        } catch {
            return Option<int>.None;
        } 
    }

    public static Option<float> ParseFloat(string text) {
        try {
            return Option<float>.Some(Convert.ToSingle(text));
        } catch {
            return Option<float>.None;
        } 
    }

    public static Option<T> ParseEnum<T>(string text) where T : struct, IConvertible {
        try {
            return Option<T>.Some((T) Enum.Parse(typeof(T), text));
        } catch {
            return Option<T>.None;
        }
    }

    public static string LogLevelToString(LogLevel logLevel) {
        return logLevel.ToString().ToLower();
    }

    public static T CastAs<T>(this object obj, T otherwise = default(T)) {
        if (obj is T casted) {
            return casted;
        }

        return otherwise;
    }

    public static bool WriteUserFile(string path, string text) {
        var full = "user://" + path;
        var file = new File();

        if (file.Open(full, (int) File.ModeFlags.Write) == Error.Ok) {
            file.StoreString(text);
            file.Close();
            return true;
        }

        return false;
    }

    public static string ReadUserFile(string path) {
        var full = "user://" + path;
        var file = new File();

        if (file.Open(full, (int) File.ModeFlags.Read) == Error.Ok) {
            var text = file.GetAsText();
            file.Close();
            return text;
        }

        return null;
    }

    public static object ReadUserJsonFile(string path) {
        var text = ReadUserFile(path);
        if (text == null) {
            return null;
        }

        return JSONParse(text);
    }

    public static object JSONParse(string text) {
        var parse = JSON.Parse("[" + text + "]");
        if (parse.Error != Error.Ok) {
            return null;
        }

        var array = (object[]) parse.Result;
        return array[0];
    }
    
    public static object JSONPrint(object obj) {
        try {
            return JSON.Print(obj, "    ", true);
        } catch {
            return null;
        }
    }

    public static string Quote(string text) {
        return $"\"{text}\"";
    }

    /*
     *
     * Iterables
     *
     */

    public static V GetOrDefault<K, V>(this IDictionary<K, V> dictionary, K key, V otherwise = default(V)) {
        if (dictionary.TryGetValue(key, out var value)) {
            return value;
        }

        return otherwise;
    }

    public static O GetOrDefault<K, V, O>(this IDictionary<K, V> dictionary, K key, O otherwise = default(O))
        where O : V {
        if (dictionary.TryGetValue(key, out var value)) {
            if (value is O output) {
                return output;
            }
        }

        return otherwise;
    }

    public static IEnumerable<T> Sorted<T>(this IEnumerable<T> enumerable) {
        var list = enumerable.ToList();
        list.Sort();
        return list;
    }

    public static void Each<T>(this IEnumerable<T> enumerable, Action<T> action) {
        foreach (var element in enumerable) {
            action(element);
        }
    }

    public static IEnumerable<int> Indices<T>(this IEnumerable<T> enumerable) {
        var i = 0;
        foreach (var _ in enumerable) {
            yield return i++;
        }
    }

    public static bool Empty<T>(this T[] array) {
        return array.Length == 0;
    }
    
    public static bool Empty<T>(this List<T> list) {
        return list.Count == 0;
    }

    public static bool Empty<T>(this IEnumerable<T> enumerable) {
        return !enumerable.Any();
    }

    public static string Join<T>(this IEnumerable<T> enumerable, string separator = "") {
        return string.Join(separator, enumerable);
    }
    
    public static T Max<T>(this IEnumerable<T> enumerable, Transform<T, int> transform) {
        var max = int.MinValue;
        var maxElement = default(T);
        var any = false;
        foreach (var element in enumerable) {
            var value = transform(element);
            if (value > max) {
                max = value;
                maxElement = element;
            }

            any = true;
        }

        if (any) {
            return maxElement;
        }

        throw new InvalidOperationException();
    }

    public static T Min<T>(this IEnumerable<T> enumerable, Transform<T, int> transform) {
        var min = int.MaxValue;
        var minElement = default(T);
        var any = false;
        foreach (var element in enumerable) {
            var value = transform(element);
            if (value < min) {
                min = value;
                minElement = element;
            }

            any = true;
        }

        if (any) {
            return minElement;
        }

        throw new InvalidOperationException();
    }

    public static IEnumerable<T> Cut<T>(this IEnumerable<T> enumerable, Predicate<T> predicate) {
        foreach (var element in enumerable) {
            if (!predicate(element)) {
                yield return element;
            }
        }
    }

    public static IEnumerable<int> Range(int count) {
        for (var i = 0; i < count; i++) {
            yield return i;
        }
    }

    public static IEnumerable<int> Range(int start, int end) {
        for (var i = start; i < end; i++) {
            yield return i;
        }
    }

    public static int Index<T>(this IEnumerable<T> enumerable, Predicate<T> predicate) {
        var i = 0;
        foreach (var element in enumerable) {
            if (predicate(element)) {
                return i;
            }

            i++;
        }

        return -1;
    }

    public static T Find<T>(this IEnumerable<T> enumerable, Predicate<T> predicate) {
        foreach (var element in enumerable) {
            if (predicate(element)) {
                return element;
            }
        }

        return default(T);
    }

    /*
     *
     * String
     *
     */

    public static string SpreadPascalCase(string text) {
        if (text.Any()) {
            return text.First() + text.Skip(1).SelectMany(c => char.IsUpper(c) ? $" {c}" : $"{c}").Join();
        }
        return "";
    }

    /*
     *
     * Networking
     *
     */

    public static string GetLocalIP() {
        try {
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0)) {
                socket.Connect("8.8.8.8", 65530);
                var end = socket.LocalEndPoint as IPEndPoint;
                return end?.Address.ToString();
            }
        } catch {
            return null;
        }
    }

    public static string[] GetAllLocalIPAddresses() {
        var name = Dns.GetHostName();
        var entry = Dns.GetHostEntry(name);
        return entry.AddressList.Select((current) => current.ToString()).ToArray();
    }

    public static int GetOpenPort(int min, int max) {
        for (var port = min; port < max; port++) {
            if (IsOpenPort(port)) {
                return port;
            }
        }

        return -1;
    }

    public static bool IsOpenPort(int port) {
        return IPGlobalProperties
               .GetIPGlobalProperties()
               .GetActiveTcpListeners()
               .All(listener => listener.Port != port);
    }

    public static bool IsIPAddress(string ip) {
        return Regex.IsMatch(ip,
                             @"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b");
    }

    public static int ParsePort(string text) {
        if (Regex.IsMatch(text, @"[0-9]+")) {
            var number = Convert.ToInt32(text);
            if (number > 8000) {
                return number;
            }
        }

        return -1;
    }

    /*
     *
     * Nodes 
     *
     */

    public static void PoliteFocus(this Control control) {
        if (control.GetFocusOwner() == null) {
            Focus(control);
        }
    }

    public static void Focus(this Control control) {
        control.GrabFocus();
        control.GrabClickFocus();
        control.FocusMode = Control.FocusModeEnum.All;
    }

    public static T Instance<T>(this PackedScene scene) where T : Node {
        return (T) scene.Instance();
    }

    public static class Action {
        public const string Jump = "jump";
        public const string Dash = "dash";
        public const string WallDrag = "wall_drag";
        public const string Activate = "activate";
        public const string MoveLeft = "move_left";
        public const string MoveRight = "move_right";
        public const string OpenMenu = "open_menu";
        public const string ToggleConsole = "toggle_console";
        public const string ToggleConsoleFullscreen = "toggle_console_fullscreen";
        public const string UITabPrevious = "ui_tab_previous";
        public const string UITabNext = "ui_tab_next";
        public const string UIUp = "ui_up";
        public const string UIDown = "ui_down";
        public const string UICancel = "ui_cancel";
        public const string UISelect = "ui_select";
        public const string EnterCommand = "enter_command";
        public const string Confirm = "confirm";
        public const string TabComplete = "tab_complete";
    }

    public static class Signal {
        public const string BodyEntered = "body_entered";
        public const string BodyExited = "body_exited";
    }
}
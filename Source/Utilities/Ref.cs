using System;
using Godot;

public class Ref<T> where T : Node {
    Node node;

    Ref(T node) {
        Set(node);
    }

    T Node {
        get {
            if (node == null) {
                return null;
            }

            if (node.NativeInstance == IntPtr.Zero) {
                node = null;
                return null;
            }

            return (T) node;
        }
    }

    public bool Exists => Node != null;
    public bool Missing => Node == null;

    public static Ref<T> Nil => new Ref<T>(null);

    public void Set(T node) {
        this.node = node;
    }

    public static Ref<T> Of(T node) {
        return new Ref<T>(node);
    }
}
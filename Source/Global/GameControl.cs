using Godot;

public enum GameState {
    Menu,
    Play
}

public enum ControlState {
    UI,
    Avatar
}

public class GameControl : GlobalControl {
    public bool InPlay => GameState == GameState.Play;
    public bool InMenu => GameState == GameState.Menu;

    public bool HasPlayerControl => ControlState == ControlState.Avatar;
    public bool HasUIControl => ControlState == ControlState.UI;

    public bool InStation => Game.Level.CurrentLevelName == "Station";

    GameState gameState = GameState.Menu;
    public GameState GameState {
        get => gameState;
        set {
            if (value != gameState) {
                gameState = value;
                OnGameStateChanged(gameState);
                GameStateChangedEvent.Emit(gameState);
            }
        }
    }

    public ControlState ControlState => InMenu || UINeedsControl ? ControlState.UI : ControlState.Avatar;
    public bool UINeedsControl;
    
    public event Action<GameState> GameStateChangedEvent;

    public override void OnStartup() {
        GameStateChangedEvent.Emit(GameState);

        Game.Network.PlayerConnectedEvent += OnPlayerConnected;
        Game.Network.PlayerDisconnectedEvent += OnPlayerDisconnected;
    }

    public void Start() {
        Restart();
        if (Game.Level.HasStandardLevel(Game.Config.StartLevel)) {
            EnterLevel(Game.Config.StartLevel, LevelType.Standard);
        }
    }

    public void Restart() {
        Game.Players.Clear();
        Game.Level.Unload();
        Game.Network.Disconnect();
        Game.Players.Create(new PlayerInfo("Local", 1, true));
        GameState = GameState.Menu;
    }

    public void Exit() {
        GetTree().Quit();
    }

    [Remote]
    public void EnterLevel(string level, LevelType type, bool local = true) {
        Game.Network.RPCSendIf(this, local, nameof(EnterLevel), level, type, false);
        GameState = GameState.Play;

        Game.Level.Load(level, type);
        Game.Players.Reset();
        Game.Level.Spawn(Game.Players);
        Game.Players.Local.Avatar.Focus();
    }

    [Remote]
    public void Respawn(bool local = true) {
        Game.Network.RPCSendIf(this, local, nameof(Respawn), false);
        Game.Level.Spawn(Game.Players);
    }

    public void ReturnToStation() {
        EnterLevel("Station", LevelType.Special);
    }

    public bool StartHosting() {
        var result = Game.Network.StartAsHost();
        if (result.Output(out var id).Failed) {
            return false;
        }

        Game.Players.Write(Game.Players.Local, new PlayerInfo("Host", id, true));
        Game.Network.GiveNetworkControl(Game.Players.Local);
        return true;
    }

    public bool ConnectToHost(string ip, int port) {
        var result = Game.Network.StartAsClient(ip, port);
        if (result.Output(out var id).Failed) {
            return false;
        }

        Game.Players.Write(Game.Players.Local, new PlayerInfo("Client", id, true));
        Game.Network.GiveNetworkControl(Game.Players.Local);
        return true;
    }

    public void DisconnectFromNetwork() {
        Game.Network.Disconnect();
        Game.Players.ClearNonLocal();
        Game.Players.Write(Game.Players.Local, new PlayerInfo("Local", 1, true));
    }

    void OnPlayerConnected(int id) {
        var other = Game.Players.Create(new PlayerInfo(id == 1 ? "Host" : "Client", id, false));
        Game.Network.GiveNetworkControl(other);
        Game.Level.Spawn(other);
        Log.Normal(other.Info.IsLocal);
    }

    void OnPlayerDisconnected(int id) {
        Game.Players.Remove(id);
    }

    void OnGameStateChanged(GameState state) {
        Log.Normal($"Game state changed to [{state}].");
        if (state == GameState.Play) {
            Game.Players.Local.Avatar.Focus();
        } 
    }
}
using Godot;
using Network = Godot.NetworkedMultiplayerENet;

/// <summary>
/// The role in which the <see cref="NetworkControl"/> is currently operating.
/// </summary>
public enum NetworkState {
    /// <summary>
    /// Signals that the user is offline and has not connected peers.
    /// </summary>
    Offline,
    
    /// <summary>
    /// Signals that the user is currently acting as a host to other players. 
    /// </summary>
    Host,
    
    /// <summary>
    /// Signals that the user is connected or connecting to another host player. 
    /// </summary>
    Client
}

/// <summary>
/// Central hub for your networking needs.
/// </summary>
public class NetworkControl : GlobalControl {
    public const int MaxPlayerCount = 4;

    [Export] public string TestFlag = "";
    
    public event Action<NetworkState> StateChangedEvent;
    public event Action<NetworkedMultiplayerPeer.ConnectionStatus> ConnectionStatusChangedEvent;
    public event Action<int> PlayerConnectedEvent;
    public event Action<int> PlayerDisconnectedEvent;
    public event Callback DisconnectedEvent;

    /// <summary>
    /// The IP address the user is either hosting on or connected to. Set to empty string when offline.
    /// </summary>
    public string IP { get; private set; } = "";
    
    /// <summary>
    /// The port the user is either hosting on or connected to. Set to -1 when offline.
    /// </summary>
    public int Port { get; private set; } = -1;

    public bool IsOnline => State != NetworkState.Offline;
    public bool IsOffline => State == NetworkState.Offline;

    /// <summary>
    /// The current state of the <see cref="NetworkControl"/>. Emits a <see cref="StateChangedEvent"/> when changed.
    /// </summary>
    public NetworkState State {
        get => state;
        set {
            if (state != value) {
                state = value;
                Log.Normal($"Network state changed to [{state}].");
                StateChangedEvent.Emit(state);
            }
        }
    }
    
    NetworkState state = NetworkState.Offline;

    /// <summary>
    /// True if the user is hosting a game or is connected to a host as a client.
    /// </summary>
    public bool HasConnection => ConnectionStatus == NetworkedMultiplayerPeer.ConnectionStatus.Connected;

    /// <summary>
    /// The current status of the network peer connection. 
    /// </summary>
    public NetworkedMultiplayerPeer.ConnectionStatus ConnectionStatus =>
        Network?.GetConnectionStatus() ?? NetworkedMultiplayerPeer.ConnectionStatus.Disconnected;
    
    Network Network;

    public override void OnStartup() {
        /*
         * Connect networking signals.
         */
        GetTree().Connect("network_peer_connected", this, nameof(OnPlayerConnected));
        GetTree().Connect("network_peer_disconnected", this, nameof(OnPlayerDisconnected));
        GetTree().Connect("connected_to_server", this, nameof(OnConnectionSuccess));
        GetTree().Connect("server_disconnected", this, nameof(OnServerDisconnected));
        GetTree().Connect("connection_failed", this, nameof(OnConnectionFailed));

        if (TestFlag == "host") {
            StartAsHost();
        } else if (TestFlag == "client") {
            StartAsClient(Constant.DefaultIP, Constant.MinNetworkPort);
        }

        /*
         * Watch the connection status and emit an event when it is changed.
         */
        Game.Task.Observe(this, () => (int) ConnectionStatus, (status) => {
            Log.Normal($"Connection status changed to [{(NetworkedMultiplayerPeer.ConnectionStatus) status}].");
            ConnectionStatusChangedEvent.Emit((NetworkedMultiplayerPeer.ConnectionStatus) status);
        });
    }

    /// <summary>
    /// Start hosting a game. Returns a <see cref="Result{T}"/> containing our network ID as the host if successful.
    /// </summary>
    public Result<int> StartAsHost() {
        Log.Normal("Starting as host.");

        Disconnect();

        Log.Normal("Starting as host.");

        var network = new NetworkedMultiplayerENet();
        var port = Ut.GetOpenPort(Constant.MinNetworkPort, Constant.MaxNetworkPort);
        var ip = Ut.GetLocalIP();

        Log.Normal($"Creating server on port [{port}].");

        var error = network.CreateServer(port, MaxPlayerCount);
        if (error == Error.Ok) {
            GetTree().SetNetworkPeer(network);
            Log.Normal($"Successfully created server on port [{port}].");
        } else {
            Log.Error($"Failed to created server on port [{port}].");
        }

        Set(network, ip, port, NetworkState.Host);

        return Result<int>.Of(GetUniqueID());
    }

    /// <summary>
    /// Begin connecting to a host at a given IP address and port. Returns a <see cref="Result{T}"/> containing our
    /// network ID if successful.
    /// </summary>
    public Result<int> StartAsClient(string ip, int port) {
        Log.Normal("Starting as client.");

        Disconnect();

        var network = new NetworkedMultiplayerENet();

        Log.Normal($"Creating client at [{ip}:{port}].");

        network.CreateClient(ip, port);
        GetTree().SetNetworkPeer(network);

        Log.Normal($"Successfully created client at [{ip}:{port}].");

        Set(network, ip, port, NetworkState.Client);

        return Result<int>.Of(GetUniqueID());
    }

    /// <summary>
    /// Run an RPC on a given node if we are online and a provided condition is true.
    /// </summary>
    public void RPCSendIf(Node node, bool condition, string function, params object[] arguments) {
        if (condition && IsOnline && HasConnection) {
            node.Rpc(function, arguments);
        }
    }

    /// <summary>
    /// Run an unreliable RPC on a given node if we are online and a provided condition is true.
    /// </summary>
    public void RPCUSendIf(Node node, bool condition, string function, params object[] arguments) {
        if (condition && IsOnline && HasConnection) {
            node.RpcUnreliable(function, arguments);
        }
    }

    /// <summary>
    /// Run an RPC on a given node if we are online. 
    /// </summary>
    public void RPCSend(Node node, string function, params object[] arguments) {
        if (IsOnline && HasConnection) {
            node.Rpc(function, arguments);
        }
    }

    /// <summary>
    /// Run an RPC on a given node if we are online. 
    /// </summary>
    public void RPCUSend(Node node, string function, params object[] arguments) {
        if (IsOnline && HasConnection) {
            node.RpcUnreliable(function, arguments);
        }
    }

    /// <summary>
    /// Set the network master of a player to their associated network ID.
    /// </summary>
    public void GiveNetworkControl(Player player) {
        player.SetNetworkMaster(player.Info.ID);
    }
    
    /// <summary>
    /// Returns true if the node is controlled by the network master. Always returns true when offline.
    /// </summary>
    public bool IsMaster(Node node) {
        return IsOffline || node.IsNetworkMaster();
    }
    
    /// <summary>
    /// Close all network connections and go offline.
    /// </summary>
    public void Disconnect() {
        Log.Normal("Disconnecting from any network.");
        if (Network == null) {
            return;
        }

        GetTree().SetNetworkPeer(null);
        Network.CloseConnection();

        Set(null, "", -1, NetworkState.Offline);

        DisconnectedEvent.Emit();
    }

    /// <summary>
    /// Return the unique network ID of the scene tree.
    /// </summary>
    int GetUniqueID() {
        return GetTree().GetNetworkUniqueId();
    }

    /// <summary>
    /// Update the network, network state and connection information.
    /// </summary>
    void Set(Network network, string ip, int port, NetworkState state) {
        Network = network;
        IP = ip;
        Port = port;
        State = state;
    }

    void OnPlayerConnected(int id) {
        Log.Normal($"Player [{id}] connected.");
        PlayerConnectedEvent.Emit(id);
    }

    void OnPlayerDisconnected(int id) {
        Log.Normal($"Player [{id}] disconnected.");
        PlayerDisconnectedEvent.Emit(id);
    }

    void OnConnectionSuccess() {
        Log.Normal("Connection successful.");
    }

    void OnServerDisconnected() {
        Log.Normal("Server disconnected.");
        Disconnect();
    }

    void OnConnectionFailed() {
        Log.Normal("Connection failed.");
        Disconnect();
    }
}
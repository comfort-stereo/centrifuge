﻿using Godot;

public class UIControl : GlobalControl {

    public Node Tree => GDU.AtLocal<Node>(this);

    public override void OnStartup() {
        Game.Control.GameStateChangedEvent += OnGameStateChanged;
    }

    public override void OnProcess(float delta) {
        Game.Control.UINeedsControl = Tree.At<PlayerMenu>().IsOpen
                                        || Tree.At<Console>().IsOpen
                                        && Tree.At<Console>().IsFocused;
    }

    void OnGameStateChanged(GameState state) {
        if (state == GameState.Menu) {
            Tree.At<Camera2D>().Current = true;
        }
    }
}

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Godot;

/// <summary>
/// A developer console command interpreter. Pretty lackluster but it works.
/// </summary>
public class CommandProcessor : GlobalControl {

    /// <summary>
    /// A mapping of all command entries to their names.
    /// </summary>
    Dictionary<string, CommandEntry> CommandEntries {
        get {
            if (commandEntries == null) {
                commandEntries = GenerateCommandEntries().ToDictionary((current) => current.Name);
            }

            return commandEntries;
        }
    }
    
    Dictionary<string, CommandEntry> commandEntries;

    /// <summary>
    /// Run a command, returning true if it was valid.
    /// </summary>
    public bool Run(string command) {
        command = command.Trim();
        if (command.Empty()) {
            return false;
        }
        
        var name = command.Trim().Split(' ').First();
        var remainder = command.Substring(name.Length);

        var converted = ConvertSyntaxToJSON(remainder);
        var arguments = Ut.JSONParse($"[{converted}]") as object[];
        if (arguments == null) {
            Log.Console("INVALID SYNTAX");
            return false;
        }

        /*
         * Check to see if the the command is defined.
         */
        var entry = GetEntry(name);
        if (entry == null) {
            Log.Console($"UNKNOWN COMMAND [{name}]");
            return false;
        }

        /*
         * Log the entered command.
         */
        Log.Console(command);

        /*
         * Invoke the command and return true if it was successful.
         */
        var success = Invoke(entry, arguments);
        if (success) {
            return true;
        }

        /*
         * Print help if we couldn't process the command.
         */
        Log.Print("COULD NOT RUN COMMAND");
        Log.Print(entry.Help());

        return false;
    }

    /// <summary>
    /// Allow writing non-whitespace strings by prepending a '@' character to a token. Convert those tokens to JSON
    /// strings.
    /// </summary>
    string ConvertSyntaxToJSON(string text) {
        var characters = new StringBuilder(text);
        
        for (var i = 0; i < characters.Length; i++) {
            if (characters[i] == '@') {
                characters[i] = '\"';
                var j = i + 1;
                while (j < characters.Length && char.IsLetterOrDigit(characters[j])) {
                    j++;
                }
                    
                if (j == characters.Length) {
                    characters.Append("\"");
                } else {
                    characters.Insert(j - 1, "\"");
                }
            } 
        }

        return characters.ToString();
    }

    /// <summary>
    /// Parse and validate the arguments provided to a command, then call the command's function with the parsed
    /// arguments if everything worked out.
    /// </summary>
    bool Invoke(CommandEntry entry, object[] arguments) {
        var parsed = entry.Parameters.Parse(arguments);
        if (parsed == null || entry.Validate != null && !entry.Validate(parsed)) {
            return false;
        }

        entry.Function.Emit(parsed);

        return true;
    }

    /// <summary>
    /// Find a defined function by name. Return null otherwise. 
    /// </summary>
    CommandEntry GetEntry(string name) {
        return CommandEntries.GetOrDefault(name);
    }

    /// <summary>
    /// Print a list of elements out to the console in a semi-fancy way. 
    /// </summary>
    void PrintList(string title, IEnumerable<object> list) {
        Log.Print($"+++++ {title} +++++");
        var elements = list.Select((element) => element.ToString()).Sorted();
        foreach (var element in elements) {
            Log.Print($"+ {element}");
        }
        
        Log.Print($"+++++ {title} +++++");
    }

    /// <summary>
    /// Return a list of defined command entries. All commands are defined here.
    /// </summary>
    CommandEntry[] GenerateCommandEntries() {
        return new[] {
            new CommandEntry {
                Name = "help",
                Function = (arguments) => PrintList("COMMANDS", CommandEntries.Values)
            },

            new CommandEntry {
                Name = "clear",
                Function = (arguments) => Game.UI.At<Console>().Clear()
            },
            
            new CommandEntry {
                Name = "clear-history",
                Function = (arguments) => Game.UI.At<Console>().ClearHistory()
            },
            
            new CommandEntry {
                Name = "exit",
                Function = (arguments) => Game.Control.Exit()
            },

            new CommandEntry {
                Name = "start",
                Function = (arguments) => Game.Control.Start()
            },

            new CommandEntry {
                Name = "restart",
                Function = (arguments) => Game.Control.Restart()
            },

            new CommandEntry {
                Name = "respawn",
                PlayMode = true,
                Function = (arguments) => Game.Control.Respawn()
            },

            new CommandEntry {
                Name = "station",
                PlayMode = true,
                Function = (arguments) => Game.Control.ReturnToStation()
            },

            new CommandEntry {
                Name = "set-log-level",
                Parameters = new ParameterList(new EnumParameter<LogLevel>("level")),
                Function = (arguments) => Game.Config.Use((config) => config.LogLevel = (LogLevel) arguments[0]),
            },

            new CommandEntry {
                Name = "set-start-level",
                Parameters = new ParameterList(new StringParameter("level")),
                Requirements = new[] {"Level exists"},
                Validate = (arguments) => Game.Level.HasStandardLevel((string) arguments[0]),
                Function = (arguments) => Game.Config.Use((config) => config.StartLevel = (string) arguments[0])
            },

            new CommandEntry {
                Name = "load-level",
                Parameters = new ParameterList(new StringParameter("level")),
                Requirements = new[] {"Level exists"},
                Validate = (arguments) => Game.Level.HasStandardLevel((string) arguments[0]),
                Function = (arguments) => Game.Control.EnterLevel((string) arguments[0], LevelType.Standard)
            },

            new CommandEntry {
                Name = "clear-start-level",
                Function = (arguments) => Game.Config.Use((config) => config.StartLevel = "")
            },

            new CommandEntry {
                Name = "levels",
                Function = (arguments) => PrintList("LEVELS", Game.Level.GetLevelNames(LevelType.Standard))
            },

            new CommandEntry {
                Name = "set-fullscreen",
                Parameters = new ParameterList(new BooleanParameter("on")),
                Function = (arguments) => Game.Config.Use((config) => config.Fullscreen = (bool) arguments[0])
            },

            new CommandEntry {
                Name = "set-developer-mode",
                Parameters = new ParameterList(new BooleanParameter("on")),
                Function = (arguments) => Game.Config.Use((config) => config.DeveloperMode = (bool) arguments[0])
            }
        };
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

public class CommandEntry {
    public Action<object[]> Function;
    public string Name = "";

    public ParameterList Parameters = new ParameterList();

    public bool PlayMode;
    public string[] Requirements = { };

    public Predicate<object[]> Validate;

    public override string ToString() {
        return $"{Name} {Parameters}";
    }

    public string Help() {
        var help = new List<string> {$"USAGE: {ToString()}"};

        if (PlayMode) {
            help.Add("PLAY MODE ONLY");
        }

        if (Requirements.Length > 0) {
            var requirements = "\n    " + Requirements.Join("\n*    ");
            help.Add($"REQUIREMENTS: {requirements}");
        }

        return help.Join("\n");
    }
}

public class ParameterList {
    Parameter[] Parameters;

    public ParameterList(params Parameter[] parameters) {
        Parameters = parameters;
    }

    public object[] Parse(object[] arguments) {
        if (arguments.Length != Parameters.Length) {
            return null;
        }

        var parsed = arguments.Zip(Parameters, (argument, parameter) => parameter.Parse(argument)).ToArray();
        return parsed.Contains(null) ? null : parsed;
    }

    public override string ToString() {
        return "<" + Parameters.Join(", ") + ">";
    }
}

public abstract class Parameter {
    public abstract string TypeString { get; }
    public abstract object Parse(object value);
    
    public string Name { get; protected set; }

    public override string ToString() {
        return $"{Name}: {TypeString}";
    }
}

public class BooleanParameter : Parameter {
    public BooleanParameter(string name) {
        Name = name;
    }

    public override string TypeString => "boolean";
    public override object Parse(object value) {
        return value is bool ? value : null;
    }
}

public class IntegerParameter : Parameter {
    bool IsPositive;

    public IntegerParameter(string name, bool positive = false) {
        Name = name;
        IsPositive = positive;
    }

    public override string TypeString => IsPositive ? "+int" : "int";

    public override object Parse(object value) {
        if (value is float && Mathf.Round((float) value) == (float) value) {
            var number = Convert.ToInt32(value);
            if (IsPositive && number < 0) {
                return null;
            }

            return number;
        }

        return null;
    }
}

public class FloatParameter : Parameter {
    bool IsPositive;

    public FloatParameter(string name, bool positive = false) {
        Name = name;
        IsPositive = positive;
    }

    public override string TypeString => IsPositive ? "+float" : "float";

    public override object Parse(object value) {
        if (value is float number) {
            if (IsPositive && number < 0) {
                return null;
            }

            return number;
        }

        return null;
    }
}

public class StringParameter : Parameter {
    public StringParameter(string name) {
        Name = name;
    }

    public override string TypeString => "string";
    
    public override object Parse(object value) {
        return value as string;
    }
}

public class EnumParameter<T> : Parameter where T : struct, IConvertible {
    public EnumParameter(string name) {
        Name = name;
    }
    
    public override string TypeString => "enum <" + Enum.GetNames(typeof(T))
                                                          .Select((name) => "@" + name).Join(" | ") + ">";

    public override object Parse(object value) {
        if (value is string text) {
            return Ut.ParseEnum<T>(text).AsObject;
        }
        
        return null;
    }
}

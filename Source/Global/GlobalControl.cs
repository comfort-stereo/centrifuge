﻿using Godot;

public abstract class GlobalControl : Node {
    public virtual void OnStartup() { }
    public virtual void OnProcess(float delta) { }
}
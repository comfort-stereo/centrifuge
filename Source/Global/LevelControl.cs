using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public enum LevelType {
    Special,
    Standard,
    Custom
}

public class LevelControl : GlobalControl {
    
    public string CurrentLevelName { get; private set; } = "";

    public Level CurrentLevel => GDU.Seek<Level>(this);

    public event Action<string> OnLevelLoaded;
    public event Action<string> OnLevelUnloaded;

    public override void OnStartup() {
        Log.Normal($"Level manager found [{GetLevelCount(LevelType.Standard)}] standard levels.");
    }

    public bool HasStandardLevel(string level) {
        return GetLevelNames(LevelType.Standard).Contains(level);
    }

    public int GetLevelCount(LevelType type) {
        return GetLevelNames(type).Length;
    }

    public string[] GetLevelNames(LevelType type) {
        return Directory.GetFiles(TypePath(type))
                        .Select(Path.GetFileNameWithoutExtension)
                        .ToArray();
    }

    public void Spawn(Player player) {
        player.Avatar.Spawn(CurrentLevel.SpawnPosition);
    }

    public void Spawn(IEnumerable<Player> players) {
        foreach (var player in players) {
            Spawn(player);
        }
    }

    public void Load(string name, LevelType type) {
        Unload();
        
        var directory = type.ToString();
        Log.Normal($"Loading {directory.ToLower()} level [{name}].");

        var level = Game.ResourceLoader.LoadInstance<Level>(LevelPath(type, name));
        if (level == null) {
            Log.Error($"Failed to load level [{name}].");
            return;
        }

        AddChild(level);

        CurrentLevelName = name;
        OnLevelLoaded.Emit(name);
    }

    public void Unload() {
        if (CurrentLevel == null) {
            return;
        }

        var name = CurrentLevelName;
        CurrentLevel.Free();
        CurrentLevelName = "";
        OnLevelUnloaded.Emit(name);
    }

    string TypePath(LevelType type) {
        if (type == LevelType.Special || type == LevelType.Standard) {
            return $"Levels/{type.ToString()}";
        }

        throw new NotImplementedException();
    }

    string LevelPath(LevelType type, string name) {
        return $"{TypePath(type)}/{name}.tscn";
    }
}
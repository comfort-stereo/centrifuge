using System.Collections.Generic;
using System.IO;
using Godot;
using Directory = System.IO.Directory;

/// <summary>
/// Resource management helper.
/// </summary>
public class ResourceLoader : Node {
    /// <summary>
    /// A mapping of the names of types to the paths of scenes.
    /// </summary>
    readonly Dictionary<string, string> TypePaths = new Dictionary<string, string>();
    
    /// <summary>
    /// Load a resource of a specified type at a provided path, returning it if successful. If the resource cannot be
    /// loaded or is not of the specified type, null is returned instead. 
    /// </summary>
    public T Load<T>(string path) where T : Resource {
        var full = "res://" + path;

        Log.Normal($"Loading resource at path [{full}].");
        
        var resource = GD.Load(full);
        if (resource == null) {
            Log.Error("Failed to load resource.");
            return null;
        }

        if (resource is T casted) {
            return casted;
        }

        Log.Error($"Resource found but was not type [{typeof(T).Name}], got [{resource.GetType().Name}]");
        return null;
    }

    /// <summary>
    /// Load an instance of the scene at a provided path, returning it if successful. If the scene cannot be loaded or
    /// is not of the specified type, null will be returned instead. 
    /// </summary>
    public T LoadInstance<T>(string path) where T : Node {
        var scene = Load<PackedScene>(path);
        if (scene == null) {
            Log.Error("Resource found but was not a scene.");
            return null;
        }

        var instance = scene.Instance();
        var casted = instance as T;
        if (casted == null) {
            Log.Error($"Instance found but was not of type [{typeof(T).Name}], got [{instance.GetType().Name}].");
            instance.QueueFree();
        }

        return casted;
    }
    
    /// <summary>
    /// Load and return the scene associated with the provided type if it exists, otherwise null will be returned
    /// instead. 
    /// </summary>
    public T LoadInstance<T>() where T : Node {
        var path = TypePaths.GetOrDefault(typeof(T).Name);
        if (path == null) {
            Log.Error($"Failed to load a scene of type [{typeof(T).Name}]");
            return null;
        }

        return LoadInstance<T>(path);
    }

    /// <summary>
    /// Map the scenes in the provided directory to the types of their root nodes. After the directory is indexed, the
    /// scenes can be loaded using <see cref="LoadInstance{T}()"/> without a path.
    /// </summary>
    public void Index(string directory) {
        var paths = GetScenePaths(directory);
        if (paths == null) {
            Log.Error($"Attempted to index scenes in invalid directory [{directory}].");
            return;
        }
        
        foreach (var path in paths) {
            var resource = Load<Resource>(path);
            if (resource is PackedScene scene) {
                var instance = scene.Instance();
                var type = instance.GetType();
                var name = type.Name;
                
                if (GDU.IsBaseGodotType(type)) {
                    continue;
                }
                
                TypePaths[name] = path;
                Log.Verbose($"Scene loader indexed scene [{path}] as type [{name}].");
                
            }
        }
    }

    /// <summary>
    /// Return the paths of all scene files in a given directory. Returns null if the directory cannot be found.
    /// </summary>
    IEnumerable<string> GetScenePaths(string directory) {
        if (Directory.Exists(directory)) {
            return Directory.EnumerateFiles(directory, "*.tscn", SearchOption.AllDirectories);
        }

        return null;
    }
}
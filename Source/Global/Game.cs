using System.Linq;
using Godot;

public class Game : Node {
    public static CommandProcessor CommandProcessor => GDU.At<CommandProcessor>(Instance); 
    public static ConfigControl Config => GDU.At<ConfigControl>(Instance); 
    public static GameControl Control => GDU.At<GameControl>(Instance);
    public static InputControl Input => GDU.At<InputControl>(Instance);
    public static LevelControl Level => GDU.At<LevelControl>(Instance);
    public static ActionListener Listener => GDU.At<ActionListener>(Instance);
    public static NetworkControl Network => GDU.At<NetworkControl>(Instance);
    public static PlayerControl Players => GDU.At<PlayerControl>(Instance);
    public static ResourceLoader ResourceLoader => GDU.At<ResourceLoader>(Instance);
    public static TaskControl Task => GDU.At<TaskControl>(Instance);
    public static UIControl UI => GDU.At<UIControl>(Instance);
    
    static Game Instance;

    GlobalControl[] Controls;

    public Game() {
        if (Instance == null) {
            Instance = this;
        }

        GDU.MissingNodeEvent += (node, message) => Log.Error(message);
        GDU.InstallFailedEvent += (node, message) => Log.Error(message);
    }

    public override void _Ready() {
        ResourceLoader.Index("Scenes/Instantiated");
        Controls = this.SeekAll<GlobalControl>().ToArray();
        foreach (var control in Controls) {
            control.OnStartup();
        }

        Control.Start();
    }

    public override void _Process(float delta) {
        foreach (var control in Controls) {
            control.OnProcess(delta);
        }
    }
}
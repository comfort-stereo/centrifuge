using Godot;

public class InputControl : GlobalControl {
    const float MinAxisMagnitude = 0.25f;
    const float SnapAxisMagnitude = 0.5f;

    public bool ControllerFocusRequired = true;

    bool isEnabled = true;
    public bool IsFocused { get; private set; }

    public float MoveAxisX { get; private set; }
    public float MoveAxisY { get; private set; }
    public float AimAxisX { get; private set; }
    public float AimAxisY { get; private set; }

    public float SnappedMoveAxisX { get; private set; }
    public float SnappedMoveAxisY { get; private set; }
    public float SnappedAimAxisX { get; private set; }
    public float SnappedAimAxisY { get; private set; }

    public float MoveRotation { get; private set; }
    public float AimRotation { get; private set; }

    public Vector2 MoveVector { get; private set; }
    public Vector2 AimVector { get; private set; }
    
    bool Processing => IsEnabled && (!ControllerFocusRequired || IsFocused);

    public bool IsEnabled {
        get => isEnabled;
        set {
            isEnabled = value;
            Update();
        }
    }

    public void Enable() {
        IsEnabled = true;
    }

    public void Disable() {
        IsEnabled = true;
    }

    public bool Tapped(string action) {
        return Processing && Input.IsActionJustPressed(action);
    }

    public bool JustReleased(string action) {
        return Processing && Input.IsActionJustReleased(action);
    }

    public bool Pressed(string action) {
        return Processing && Input.IsActionPressed(action);
    }

    public override void OnProcess(float delta) {
        Update();
    }

    public override void _Notification(int type) {
        if (type == MainLoop.NotificationWmFocusIn) {
            IsFocused = true;
        } else if (type == MainLoop.NotificationWmFocusOut) {
            IsFocused = false;
        }
    }

    public float GetAxis(JoystickList axis) {
        var direction = Processing ? Input.GetJoyAxis(0, (int) axis) : 0f;

        if (Mathf.Abs(direction) > MinAxisMagnitude) {
            if (direction > SnapAxisMagnitude) {
                return 1;
            }

            if (direction < -SnapAxisMagnitude) {
                return -1;
            }

            return direction;
        }

        return 0;
    }

    void Update() {
        MoveAxisX = GetAxis(JoystickList.Axis0);
        MoveAxisY = GetAxis(JoystickList.Axis1);
        AimAxisX = GetAxis(JoystickList.Axis2);
        AimAxisY = GetAxis(JoystickList.Axis3);
        SnappedMoveAxisX = Mathf.Abs(MoveAxisX) < SnapAxisMagnitude ? 0 : Mathf.Sign(MoveAxisX);
        SnappedMoveAxisY = Mathf.Abs(MoveAxisY) < SnapAxisMagnitude ? 0 : Mathf.Sign(MoveAxisY);
        SnappedAimAxisX = Mathf.Abs(AimAxisX) < SnapAxisMagnitude ? 0 : Mathf.Sign(AimAxisX);
        SnappedAimAxisY = Mathf.Abs(AimAxisX) < SnapAxisMagnitude ? 0 : Mathf.Sign(AimAxisX);
        var move = Vector.Of(MoveAxisX, MoveAxisY);
        var aim = Vector.Of(AimAxisX, AimAxisY);
        var hasMove = MoveAxisX != 0 || MoveAxisY != 0;
        var hasAim = AimAxisX != 0 || AimAxisY != 0;
        MoveRotation = Processing && hasMove ? Ut.RotationSnap(Mathf.Rad2Deg(move.Angle()), 45) : 0;
        AimRotation = Processing && hasAim ? Ut.RotationSnap(Mathf.Rad2Deg(aim.Angle()), 45) : 0;
        MoveVector = hasMove ? Vector.Right.Rotated(Mathf.Deg2Rad(MoveRotation)) : Vector.Zero;
        AimVector = hasAim ? Vector.Right.Rotated(Mathf.Deg2Rad(AimRotation)) : Vector.Zero;
    }
}
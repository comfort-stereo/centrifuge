using System;
using System.Linq;
using Godot;
using JSONObject = System.Collections.Generic.Dictionary<object, object>;
using JSONArray = System.Array;
using JSONInt = System.Single;

public class ConfigControl : GlobalControl {
    public const string SettingsFile = "settings.json";
    public const string StorageFile = "storage.json";

    public string[] CommandHistory = { };
    
    public bool DeveloperMode;
    public bool Fullscreen = true;
    
    public string LastIPConnected = Constant.DefaultIP;
    public int LastPortConnected = Constant.DefaultPort;
    public LogLevel LogLevel = LogLevel.Normal;

    public string PlayerName = Constant.DefaultPlayerName;
    public string StartLevel = "";
    
    public bool IsLoaded { get; private set; }

    public override void OnStartup() {
        Ensure();
        Game.Task.Observe(this, () => Fullscreen, (fullscreen) => OS.WindowFullscreen = fullscreen);
    }

    public void Use(Action<ConfigControl> action) {
        Ensure();
        action(this);
        Write();
    }

    public void Ensure() {
        if (!IsLoaded) {
            Load();
        }
    }

    public void Load() {
        Log.Normal($"Loading user data from [{OS.GetUserDataDir()}].");

        LoadSettings();
        LoadStorage();

        IsLoaded = true;
    }

    public void Write() {
        Log.Verbose("Writing the following user settings...");
        PrintSettings();

        Ut.WriteUserFile(SettingsFile, Stringify(ToSettingsJSON()));

        Log.Verbose("Writing storage...");
        Ut.WriteUserFile(StorageFile, Stringify(ToStorageJSON()));

        Log.Verbose("User data was stored successfully.");
    }

    public JSONObject ToSettingsJSON() {
        return new JSONObject {
            {"player-name", PlayerName},
            {"last-ip-connected", LastIPConnected},
            {"last-port-connected", LastPortConnected},
            {"fullscreen", Fullscreen}
        };
    }

    public JSONObject ToStorageJSON() {
        return new JSONObject {
            {"command-history", CommandHistory},
            {"log-level", Ut.LogLevelToString(LogLevel)},
            {"developer-mode", DeveloperMode.ToString().ToLower()},
            {"start-level", StartLevel}
        };
    }

    void LoadSettings() {
        Log.Normal("Loading settings...");

        var settings = Read(SettingsFile);

        PlayerName = GetString(settings, "player-name", PlayerName);
        LastIPConnected = GetString(settings, "last-ip-connected", LastIPConnected);
        LastPortConnected = GetInteger(settings, "last-port-connected", LastPortConnected);
        Fullscreen = GetBoolean(settings, "fullscreen", Fullscreen);

        PrintSettings();
    }

    void LoadStorage() {
        Log.Normal("Loading storage...");

        var storage = Read(StorageFile);
        
        CommandHistory = GetBy(storage, "command-history", new string[] { }, (history) => {
            return history.CastAs<object[]>().Cast<string>().ToArray();
        });

        LogLevel = GetBy(storage, "log-level", LogLevel, (level) => ParseLogLevel((string) level));
        DeveloperMode = GetBoolean(storage, "developer-mode", DeveloperMode);
        StartLevel = GetString(storage, "start-level", StartLevel);

        PrintStorage();
    }

    JSONObject Read(string path) {
        if (!(Ut.ReadUserJsonFile(path) is JSONObject data)) {
            data = new JSONObject();
            Ut.WriteUserFile(path, "{}");
        }

        return data;
    }

    void PrintSettings() {
        Log.Verbose("CURRENT USER SETTINGS");
        Log.Verbose("*****");
        Log.Verbose(Stringify(ToSettingsJSON()));
        Log.Verbose("*****");
    }

    void PrintStorage() {
        Log.Verbose("CURRENT STORAGE DATA");
        Log.Verbose("*****");
        Log.Verbose(Stringify(ToStorageJSON()));
        Log.Verbose("*****");
    }

    string Stringify(JSONObject data) {
        return JSON.Print(data, "    ", true);
    }

    T GetBy<T>(JSONObject data, string key, T otherwise, Transform<object, T> transform) {
        if (data.TryGetValue(key, out var value)) {
            try {
                return transform(value);
            } catch {
                return otherwise;
            }
        }

        return otherwise;
    }

    bool GetBoolean(JSONObject data, string key, bool otherwise) {
        return GetBy(data, key, otherwise, (boolean) => (bool) boolean);
    }

    string GetString(JSONObject data, string key, string otherwise) {
        return GetBy(data, key, otherwise, (text) => (string) text);
    }

    int GetInteger(JSONObject data, string key, int otherwise) {
        return GetBy(data, key, otherwise, (integer) => (int) integer);
    }

    LogLevel ParseLogLevel(string text) {
        if (text == "normal") {
            return LogLevel.Normal;
        }

        if (text == "verbose") {
            return LogLevel.Verbose;
        }

        if (text == "error") {
            return LogLevel.Error;
        }

        throw new InvalidOperationException();
    }
}
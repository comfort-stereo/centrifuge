using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Godot;

/// <summary>
/// Manager class for players. Handles creation, updates and deletion.
/// </summary>
public class PlayerControl : GlobalControl, IEnumerable<Player> {
    
    /// <summary>
    /// Reference to the player scene for creation. 
    /// </summary>
    [Export] PackedScene ScenePlayer;
    
    /// <summary>
    /// Node to store players in the scene tree.
    /// </summary>
    [Install] readonly Node List;
    
    /// <summary>
    /// The current number of connected players.
    /// </summary>
    public int Count => List.GetChildCount();

    /// <summary>
    /// The player directly controlled by the user.
    /// </summary>
    public Player Local {
        get {
            foreach (var player in this) {
                if (player.Info.IsLocal) {
                    return player;
                }
            }

            return null;
        }
    }

    /// <summary>
    /// Event emitted when any player is added, removed or modified.
    /// </summary>
    public event Action<IEnumerable<Player>> UpdateEvent;
    
    /// <summary>
    /// Create a new player with the provided info.
    /// </summary>
    public Player Create(PlayerInfo info) {
        if (Count == Constant.MaxPlayerCount) {
            return null;
        }

        Log.Normal($"Creating player [{info.Name}], ID {info.ID}");

        var player = ScenePlayer.Instance<Player>();
        Write(player, info);
        player.Avatar.Disable();
        List.AddChild(player);

        UpdateEvent.Emit(this);
        
        return player;
    }

    /// <summary>
    /// Get a player by their network ID if present. Otherwise null is returned. 
    /// </summary>
    public Player Get(int id) {
        foreach (var player in this) {
            if (player.Info.ID == id) {
                return player;
            }
        }

        return null;
    }

    /// <summary>
    /// Write new info to a player. 
    /// </summary>
    public void Write(Player player, PlayerInfo info) {
        player.Write(info);
        UpdateEvent.Emit(this);
    }

    /// <summary>
    /// Disable all player associated objects. 
    /// </summary>
    public void Reset() {
        foreach (var player in this) {
            player.Avatar.Disable();
        }
    }

    /// <summary>
    /// Remove a player. 
    /// </summary>
    public void Remove(Player player) {
        List.RemoveChild(player);
        UpdateEvent.Emit(this);
    }

    /// <summary>
    /// Remove a player by their network ID.
    /// </summary>
    public void Remove(int id) {
        var player = Get(id);
        if (player != null) {
            List.RemoveChild(player);
        }

        UpdateEvent.Emit(this);
    }

    /// <summary>
    /// Remove all players. 
    /// </summary>
    public void Clear() {
        foreach (var player in this.ToArray()) {
            List.RemoveChild(player);
        }
        UpdateEvent.Emit(this);
    }

    /// <summary>
    /// Remove all players except the local player. 
    /// </summary>
    public void ClearNonLocal() {
        foreach (var player in this.ToArray().Cut((player) => player.Info.IsLocal)) {
            Remove(player);
        }
    }
    
    /// <summary>
    /// Iterate over all connected players. 
    /// </summary>
    public IEnumerator<Player> GetEnumerator() {
        for (var i = 0; i < Count; i++) {
            yield return (Player) List.GetChild(i);
        }
    }

    /// <summary>
    /// Iterate over all connected players. 
    /// </summary>
    IEnumerator IEnumerable.GetEnumerator() {
        return GetEnumerator();
    }
}
using System;
using System.Collections.Generic;
using Godot;

public class TaskControl : GlobalControl {

    readonly List<Task> Tasks = new List<Task>();
    readonly List<Task> AddedTasks = new List<Task>();
    readonly List<Task> RemovedTasks = new List<Task>();

    public override void OnProcess(float delta) {
        UpdateTimerTasks();
    }

    void UpdateTimerTasks() {
        Tasks.AddRange(AddedTasks);
        AddedTasks.Clear();
        
        foreach (var task in Tasks) {
            var now = Ut.Time;
            if (task.Start.CompareTo(now) <= 0) {
                var duration = (task.End - task.Start).Milliseconds;
                var remaining = (task.End - now).Milliseconds;
                var progress = 1f - remaining / (float) duration;
                if (progress >= 1) {
                    progress = 1;
                }

                if (task.Owner.Missing || task.OnUpdate(progress) || progress >= 1) {
                    RemovedTasks.Add(task);
                    if (task.Owner.Exists) {
                        task.OnEnd.Emit();
                    }
                }
            }
        }

        foreach (var task in RemovedTasks) {
            Tasks.Remove(task);
        }

        RemovedTasks.Clear();
    }

    public void After(Node owner, int delay, Callback callback) {
        AddedTasks.Add(new Task(owner, delay, 0, (progress) => {
            callback();
            return true;
        }));
    }
    
    public void Repeat(Node owner, Callback callback) {
        AddedTasks.Add(new Task(owner, 0, int.MaxValue, (progress) => {
            callback();
            return false;
        }));
    }
    
    public void Repeat(Node owner, Predicate callback) {
        AddedTasks.Add(new Task(owner, 0, int.MaxValue, (progress) => {
            return callback();
        }));
    }
    
    public void RepeatAfter(Node owner, int delay, Predicate callback) {
        AddedTasks.Add(new Task(owner, delay, int.MaxValue, (progress) => {
            return callback();
        }));
    }
    
    public void Duration(Node owner, int duration, Predicate<float> callback, Callback onEnd = null) {
        if (callback(0) || duration <= 0) {
            if (onEnd != null) {
                onEnd();
            }
        } else {
            AddedTasks.Add(new Task(owner, 0, duration, callback, onEnd));
        }
    }

    public void Duration(Node owner, int duration, Action<float> callback, Callback onEnd = null) {
        Duration(owner, duration, (progress) => {
            callback(progress);
            return false;
        }, onEnd);
    }

    public void DelayedObserve<T>(Node node, Callback<T> onFetch, Action<T> onChange) where T : IEquatable<T> {
        var previous = onFetch();
        Repeat(node, () => {
            var current = onFetch();
            if (current.Equals(previous)) { } else {
                onChange(current);
                previous = current;
            }
        });
    }

    public void Observe<T>(Node node, Callback<T> onFetch, Action<T> onChange) where T : IEquatable<T> {
        onChange(onFetch());
        DelayedObserve(node, onFetch, onChange);
    }
}

public class Task {
    public readonly Ref<Node> Owner = Ref<Node>.Nil;
    public readonly DateTime End;
    public readonly Callback OnEnd;
    public readonly Predicate<float> OnUpdate;
    public readonly DateTime Start;

    public Task(Node owner, int delay, int duration, Predicate<float> callback, Callback onEnd = null) {
        Owner.Set(owner);
        Start = DateTime.Now.AddMilliseconds(delay);
        End = Start.AddMilliseconds(duration);
        OnUpdate = callback;
        OnEnd = onEnd;
    }
}
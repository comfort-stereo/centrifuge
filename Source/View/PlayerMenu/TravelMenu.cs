using Godot;

public class TravelMenu : View {
    [Install] readonly Button BackButton;
    [Install] readonly ItemList LevelList;

    protected override bool ControlsOwnVisibility => false;

    public event Callback ExitedEvent;
    public event Action<string> LevelChosenEvent;

    protected override void OnStart() {
        BackButton.Connect("pressed", this, nameof(OnBackButtonPressed));
        LevelList.Connect("item_activated", this, nameof(OnLevelChosen));
    }

    protected override void OnEnter() {
        UpdateLevelList();
        if (LevelList.GetItemCount() > 0) {
            LevelList.Select(0);
        }

        BackButton.Focus();
    }

    protected override void OnUpdate(float delta) {
        if (Game.Input.Tapped(Ut.Action.UICancel)) {
            if (BackButton.GetFocusMode() == FocusModeEnum.None) {
                BackButton.Focus();
            } else {
                ExitedEvent.Emit();
            }
        }
    }

    void UpdateLevelList() {
        LevelList.Clear();
        foreach (var level in Game.Level.GetLevelNames(LevelType.Standard)) {
            LevelList.AddItem(level, Game.ResourceLoader.Load<Texture>("Placeholder/Icon.png"));
        }
    }

    void OnLevelChosen(int index) {
        LevelChosenEvent.Emit(LevelList.GetItemText(index));
    }

    void OnBackButtonPressed() {
        ExitedEvent.Emit();
    }
}
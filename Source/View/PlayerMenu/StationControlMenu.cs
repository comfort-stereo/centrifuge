using Godot;

public class StationControlMenu : View {
    [Install] readonly Button BackButton;
    [Install] readonly Button TravelButton;

    protected override bool ControlsOwnVisibility => false;

    public event Callback TravelSelectedEvent;
    public event Callback ExitedEvent;

    protected override void OnStart() {
        TravelButton.Connect("pressed", this, nameof(OnTravelButtonPressed));
        BackButton.Connect("pressed", this, nameof(OnBackButtonPressed));
    }

    protected override void OnEnter() {
        TravelButton.Focus();
    }

    protected override void OnUpdate(float delta) {
        if (Game.Input.Tapped(Ut.Action.UICancel)) {
            ExitedEvent.Emit();
        }
    }

    void OnTravelButtonPressed() {
        TravelSelectedEvent.Emit();
    }

    void OnBackButtonPressed() {
        ExitedEvent.Emit();
    }
}
using System.Collections.Generic;
using System.Linq;
using Godot;

public class PlayerMenu : View {
    public const int StandardMenuCount = 2;

    [Install] readonly GameMenu GameMenu;
    [Install] readonly MultiplayerMenu MultiplayerMenu;
    [Install] readonly TravelMenu TravelMenu;
    
    [Install] readonly Button NextMenuButton;
    [Install] readonly Button PreviousMenuButton;
    [Install] readonly StationControlMenu StationControlMenu;
    
    [Install] readonly Label CurrentMenuLabel;
    [Install] readonly TabContainer Tabs;

    int CurrentTab {
        get => Tabs.CurrentTab;
        set => Tabs.CurrentTab = value;
    }

    int NextStandardTab => (CurrentTab + 1) % StandardMenuCount;
    int PreviousStandardTab => CurrentTab == 0 ? StandardMenuCount - 1 : CurrentTab - 1;

    View NextStandardMenu => (View) Tabs.GetTabControl(NextStandardTab);
    View PreviousStandardMenu => (View) Tabs.GetTabControl(PreviousStandardTab);

    bool CurrentMenuIsStandard => CurrentTab < StandardMenuCount;

    View CurrentMenu => (View) Tabs.GetCurrentTabControl();
    IEnumerable<View> Menus => Tabs.GetChildren().OfType<View>();

    protected override bool HideOnExit => false;
    protected override bool IsActive => Game.Control.InPlay && PreferOpen;

    protected override void OnStart() {
        PreviousMenuButton.Connect("pressed", this, nameof(OnPreviousMenuButtonPressed));
        NextMenuButton.Connect("pressed", this, nameof(OnNextMenuButtonPressed));

        Game.Level.OnLevelLoaded += (level) => { Close(); };

        GameMenu.ReturnToStationEvent += () => {
            Close();
            Game.Control.ReturnToStation();
        };
        GameMenu.ExitToMenuEvent += () => {
            Close();
            Game.Control.Restart();
        };

        StationControlMenu.TravelSelectedEvent += () => ShowMenu(TravelMenu);
        StationControlMenu.ExitedEvent += Close;

        TravelMenu.LevelChosenEvent += (level) => {
            Close();
            Game.Control.EnterLevel(level, LevelType.Standard);
        };
        TravelMenu.ExitedEvent += () => ShowMenu(StationControlMenu);
    }

    protected override void OnProcess(float delta) {
        if (Game.Control.InMenu) {
            PreferOpen = false;
        }

        if (Game.Input.Tapped(Ut.Action.OpenMenu)) {
            if (CurrentMenuIsStandard || !IsOpen) {
                Toggle();
            }
        }
    }

    protected override void OnUpdate(float delta) {
        if (CurrentMenuIsStandard) {
            if (Game.Input.Tapped(Ut.Action.UICancel)) {
                if (CurrentMenu != GameMenu) {
                    ShowMenu(GameMenu);
                } else {
                    Close();
                }
            }

            if (Game.Input.Tapped(Ut.Action.UITabPrevious)) {
                ShowPrevious();
            }

            if (Game.Input.Tapped(Ut.Action.UITabNext)) {
                ShowNext();
            }
        }
    }

    protected override void OnEnter() {
        ShowMenu(CurrentMenu);
        SetScale(Vector.Of(1, 0));
        Game.Task.Duration(this, 100, (progress) => SetScale(Vector.Of(1, Mathf.Sqrt(progress))));
    }

    protected override void OnExit() {
        SetScale(Vector.Of(1, 1));
        Game.Task.Duration(this, 50, (progress) => SetScale(Vector.Of(1, 1 - progress * progress)), Hide);
    }

    public void ShowPrevious() {
        ShowMenu(PreviousStandardMenu);
    }

    public void ShowNext() {
        ShowMenu(NextStandardMenu);
    }

    public void OpenTo<T>() where T : View {
        var menu = GDU.At<T>(this);
        if (menu != null) {
            PreferOpen = true;
            ShowMenu(menu);
        }
    }

    public void Toggle() {
        PreferOpen = !PreferOpen;
        if (!CurrentMenuIsStandard) {
            ShowMenu(GameMenu);
        }
    }

    public void Close() {
        PreferOpen = false;
    }

    void ShowMenu(View menu) {
        var tab = Menus.Index((other) => menu == other);
        if (tab < 0) {
            return;
        }

        CurrentMenu.Exit();
        CurrentTab = tab;
        CurrentMenu.Enter();
        UpdateControls();
    }

    void UpdateControls() {
        PreviousMenuButton.Visible = CurrentMenuIsStandard;
        NextMenuButton.Visible = CurrentMenuIsStandard;

        CurrentMenuLabel.SetText(Ut.SpreadPascalCase(CurrentMenu.Name));

        if (CurrentMenuIsStandard) {
            PreviousMenuButton.SetText($"<< {Ut.SpreadPascalCase(PreviousStandardMenu.Name)}");
            NextMenuButton.SetText($"{Ut.SpreadPascalCase(NextStandardMenu.Name)} >>");
        }
    }

    void OnPreviousMenuButtonPressed() {
        ShowPrevious();
    }

    void OnNextMenuButtonPressed() {
        ShowNext();
    }
}
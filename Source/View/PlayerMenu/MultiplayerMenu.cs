using Godot;

public class MultiplayerMenu : View {
    [Install] readonly Button DisconnectButton;
    [Install] readonly Button HostButton;
    [Install] readonly LineEdit IPAddressField;
    [Install] readonly Button JoinButton;
    [Install] readonly Label MessageLabel;
    [Install] readonly LineEdit PortField;

    protected override bool ControlsOwnVisibility => false;

    protected override void OnStart() {
        HostButton.Connect("pressed", this, nameof(OnHostButtonPressed));
        JoinButton.Connect("pressed", this, nameof(OnJoinButtonPressed));
        DisconnectButton.Connect("pressed", this, nameof(OnDisconnectButtonPressed));

        Game.Config.Use((config) => {
            IPAddressField.Text = config.LastIPConnected;
            PortField.Text = config.LastPortConnected.ToString();
        });

        Game.Level.OnLevelLoaded += (level) => UpdateControls();
        Game.Network.StateChangedEvent += (state) => UpdateControls();
    }

    protected override void OnEnter() {
        JoinButton.PoliteFocus();
    }

    void OnHostButtonPressed() {
        Game.Control.StartHosting();
    }

    void OnJoinButtonPressed() {
        var ip = Ut.IsIPAddress(IPAddressField.Text) ? IPAddressField.Text : null;
        var port = Ut.ParsePort(PortField.Text);

        if (ip == null) {
            MessageLabel.Text = "IP address is not correctly formed.";
            return;
        }

        if (port < 0) {
            MessageLabel.Text = "Port number is not correctly formed, must a positive integer > 8000.";
            return;
        }

        Game.Config.LastIPConnected = ip;
        Game.Config.LastPortConnected = port;
        Game.Config.Write();

        if (Game.Control.ConnectToHost(ip, port)) { } else {
            MessageLabel.Text = "Failed to connect.";
        }
    }

    void OnDisconnectButtonPressed() {
        Game.Control.DisconnectFromNetwork();
    }

    void UpdateControls() {
        HostButton.Disabled = Game.Network.IsOnline || !Game.Control.InStation;
        JoinButton.Disabled = Game.Network.IsOnline || !Game.Control.InStation;
        DisconnectButton.Disabled = !Game.Network.HasConnection && Game.Network.State != NetworkState.Client;

        IPAddressField.Editable = Game.Network.IsOffline && Game.Control.InStation;
        PortField.Editable = Game.Network.IsOffline && Game.Control.InStation;

        if (Game.Network.IsOnline) {
            var ip = Game.Network.IP;
            var port = Game.Network.Port;
            if (Game.Network.State == NetworkState.Host) {
                MessageLabel.Text = $"Hosting at ip {ip} on port {port}.";
            }

            if (Game.Network.State == NetworkState.Client) {
                MessageLabel.Text = Game.Network.HasConnection
                    ? $"Client at ip {ip} on port {port}."
                    : $"Joining host at ip {ip} on port {port}.";
            }
        } else {
            MessageLabel.Text = "Offline";
        }
    }
}
using Godot;

public class GameMenu : View {
    [Install] readonly Button ExitToMenuButton;
    [Install] readonly Button ReturnToStationButton;

    protected override bool ControlsOwnVisibility => false;

    public event Callback ReturnToStationEvent;
    public event Callback ExitToMenuEvent;

    protected override void OnStart() {
        ReturnToStationButton.Connect("pressed", this, nameof(OnReturnToStationButtonPressed));
        ExitToMenuButton.Connect("pressed", this, nameof(OnExitToMenuButtonPressed));
    }

    protected override void OnEnter() {
        ExitToMenuButton.PoliteFocus();
    }

    void OnReturnToStationButtonPressed() {
        ReturnToStationEvent.Emit();
    }

    void OnExitToMenuButtonPressed() {
        ExitToMenuEvent.Emit();
    }
}
using System.Collections.Generic;
using Godot;

public class PlayerList : View {
    [Install] readonly ItemList List;
    [Export] readonly Texture Icon; 

    protected override bool IsActive => Game.Control.InPlay && Game.Network.IsOnline;
    protected override bool HideOnExit => false;

    protected override void OnStart() {
        Game.Players.UpdateEvent += Update;
    }

    protected override void OnEnter() {
        Game.Task.Duration(this, 100, (progress) => SetScale(Vector.Of(Mathf.Sqrt(progress), 1)));
    }

    protected override void OnExit() {
        Game.Task.Duration(this, 50, (progress) => SetScale(Vector.Of(progress * progress, 1)), Hide);
    }

    void Update(IEnumerable<Player> players) {
        List.Clear();
        foreach (var player in players) {
            List.AddItem(player.Info.Name, Icon, false);
        }
    }
}
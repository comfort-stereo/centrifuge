using Godot;

public abstract class View : Container {
    bool isOpen;

    protected virtual bool HideOnExit => true;
    protected virtual bool IsActive => PreferOpen;
    protected virtual bool ControlsOwnVisibility => true;

    public bool PreferOpen { get; protected set; } = true;

    public bool IsOpen {
        get => isOpen;
        set {
            if (value) {
                Enter();
            } else {
                Exit();
            }

            isOpen = value;
        }
    }

    public override void _Ready() {
        Exit(true);
        OnStart();
        Exit(true);
    }

    public override void _Process(float delta) {
        if (ControlsOwnVisibility) {
            IsOpen = IsActive;
        }

        OnProcess(delta);
        if (IsOpen) {
            OnUpdate(delta);
        }
    }

    public void Enter(bool force = false) {
        if (!isOpen || force) {
            Log.Verbose($"Entering view [{Name}].");

            Show();
            OnEnter();

            isOpen = true;
        }
    }

    public void Exit(bool force = false) {
        if (isOpen || force) {
            Log.Verbose($"Exiting view [{Name}].");
            if (force || HideOnExit) {
                Hide();
            }

            OnExit();

            isOpen = false;
        }
    }

    protected virtual void OnStart() { }
    protected virtual void OnEnter() { }
    protected virtual void OnExit() { }
    protected virtual void OnProcess(float delta) { }
    protected virtual void OnUpdate(float delta) { }
}
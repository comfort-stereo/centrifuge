using Godot;

public class MainMenu : View {
    [Install] readonly Button ExitButton;
    [Install] readonly Button PlayButton;
    [Install] readonly Button SettingsButton;

    protected override bool IsActive => Game.Control.InMenu;

    protected override void OnStart() {
        PlayButton.Connect("pressed", this, nameof(OnPlayButtonPressed));
        SettingsButton.Connect("pressed", this, nameof(OnSettingsButtonPressed));
        ExitButton.Connect("pressed", this, nameof(OnExitButtonPressed));
    }

    protected override void OnEnter() {
        PlayButton.GrabFocus();
        Game.Task.Duration(this, 500, (progress) => {
            var scale = Vector.Of(Mathf.Sqrt(progress), 1);
            PlayButton.SetScale(scale);
            SettingsButton.SetScale(scale);
            ExitButton.SetScale(scale);
        });
    }

    protected override void OnExit() { }

    void OnPlayButtonPressed() {
        Game.Control.ReturnToStation();
    }

    void OnSettingsButtonPressed() { }

    void OnExitButtonPressed() {
        Game.Control.Exit();
    }
}
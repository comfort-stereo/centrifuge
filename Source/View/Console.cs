using Godot;

public class Console : View {
    const float ExpandedHeight = 800;
    const int MaxCharacterCount = 10000;
    const int DropCharacterCount = 1000;

    [Install] readonly LineEdit CommandLine;
    [Install] readonly TextEdit OutputView;
    
    readonly History History = new History();

    int CharacterCount;

    bool IsExpanded;
    Control PreviousFocusOwner;
    float UnexpandedHeight;

    public bool IsFocused => CommandLine.FocusMode == FocusModeEnum.Click;

    protected override bool IsActive => PreferOpen;

    protected override void OnStart() {
        CommandLine.Connect("text_entered", this, nameof(OnCommandEntered));
        CommandLine.Connect("text_changed", this, nameof(OnCommandChanged));

        Game.Config.Use((config) => History.Write(config.CommandHistory));

        OutputView.AddKeywordColor("LOG", Color.Color8(0, 255, 0, 255));
        OutputView.AddKeywordColor("ERROR", Color.Color8(255, 0, 0, 255));
        OutputView.AddKeywordColor("CONSOLE", Color.Color8(0, 255, 255, 255));
        OutputView.AddKeywordColor(":", Color.Color8(255, 255, 255, 255));
        OutputView.AddColorRegion("\"", "\"", Color.Color8(0, 255, 0, 255));

        PreferOpen = false;
        UnexpandedHeight = RectSize.y;

        Log.MessageLoggedEvent += Print;
        Log.ErrorLoggedEvent += (message) => {
            PreferOpen = true;
        };
    }

    protected override void OnEnter() {
        CommandLine.GrabFocus();
        CommandLine.SetCursorPosition(CommandLine.Text.Length);
    }

    protected override void OnExit() {
        CommandLine.FocusMode = FocusModeEnum.None;
    }

    protected override void OnProcess(float delta) {
        if (Game.Input.Tapped(Ut.Action.ToggleConsole)) {
            Toggle();
        }

        if (Game.Input.Tapped(Ut.Action.EnterCommand)) {
            OnCommandEntered(CommandLine.Text);
        }
    }

    protected override void OnUpdate(float delta) {
        if (IsFocused) {
            if (Game.Input.Tapped(Ut.Action.ToggleConsoleFullscreen)) {
                ToggleFullscreen();
            }

            if (Game.Input.Tapped(Ut.Action.UIUp)) {
                HistoryPrevious();
            }

            if (Game.Input.Tapped(Ut.Action.UIDown)) {
                HistoryNext();
            }

            if (Game.Input.Tapped(Ut.Action.UICancel)) {
                UnfocusCommandLine();
            }

            if (Game.Input.Tapped(Ut.Action.Confirm)) {
                OnCommandEntered(CommandLine.Text);
            }

            if (Game.Input.Tapped(Ut.Action.TabComplete)) {
                Complete();
            }
        }

        RectSize = RectSize.SetY(IsExpanded ? ExpandedHeight : UnexpandedHeight);
    }

    public void Print(string message) {
        OutputView.InsertTextAtCursor(message);
        OutputView.InsertTextAtCursor("\n");

        CharacterCount += message.Length + 1;
        if (CharacterCount > MaxCharacterCount) {
            var text = OutputView.Text.Substring(DropCharacterCount);
            OutputView.Text = text;
            CharacterCount = text.Length;
        }

        ResetOutputCursor();
    }

    public void Clear() {
        OutputView.Text = "";
        ResetOutputCursor();
    }

    public void ClearHistory() {
        History.Clear();
        Game.Config.Use((config) => {
            config.CommandHistory = new string[] { };
        });
    }

    void Toggle() {
        if (PreferOpen && IsFocused) {
            PreferOpen = false;
            CommandLine.FocusMode = FocusModeEnum.None;
            PreviousFocusOwner?.Focus();
        } else {
            PreferOpen = true;
            PreviousFocusOwner = GetFocusOwner();
            FocusCommandLine();
        }
    }

    void ToggleFullscreen() {
        IsExpanded = !IsExpanded;
    }

    void FocusCommandLine() {
        CommandLine.FocusMode = FocusModeEnum.Click;
        CommandLine.GrabClickFocus();
        CommandLine.GrabFocus();
        CommandLine.SetCursorPosition(CommandLine.Text.Length);
    }

    void UnfocusCommandLine() {
        OutputView.Focus();
        CommandLine.FocusMode = FocusModeEnum.None;
    }

    void Complete() {
        if (CommandLine.FocusMode != FocusModeEnum.None) {
            if (CommandLine.Text.Length > 0) {
                var completed = History.Complete(CommandLine.Text);
                if (completed != null) {
                    CommandLine.Text = completed;
                }
            }

            FocusCommandLine();
        }
    }

    void OnCommandEntered(string command) {
        CommandLine.Clear();

        command = command.Trim();
        if (command.Length == 0) {
            if (History.Size > 0) {
                OnCommandEntered(History.Latest());
                History.Reset();
            }

            return;
        }

        var accepted = Game.CommandProcessor.Run(command);
        History.Reset();
        if (accepted) {
            RecordHistory(command);
        }
    }

    void HistoryPrevious() {
        CommandLine.Text = History.Previous();
        FocusCommandLine();
    }

    void HistoryNext() {
        CommandLine.Text = History.Next();
        FocusCommandLine();
    }

    void RecordHistory(string command) {
        History.Add(command);

        Game.Config.CommandHistory = History.Export();
        Game.Config.Write();
    }

    void OnCommandChanged(string text) {
        if (text.Length > 0 && text[text.Length - 1] == '`') {
            CommandLine.Text = text.Substring(0, text.Length - 1);
        }
    }

    void ResetOutputCursor() {
        Game.Task.Duration(this, 25, (progress) => {
            OutputView.CursorSetColumn(int.MaxValue);
            OutputView.CursorSetLine(int.MaxValue);
        });
    }
}
﻿using Godot;

public class StatDisplay : View {
    const int StaminaBarChangeRate = 10;

    [Install] TextureProgress StaminaBar;
    
    protected override bool IsActive => Game.Control.InPlay;
    
    protected override void OnUpdate(float delta) {
        UpdateStamina(delta);
    }

    void UpdateStamina(float delta) {
        var stamina = Game.Players.Local.Avatar.Stamina;
        var change = StaminaBarChangeRate * delta;
        StaminaBar.Value = Mathf.Lerp(StaminaBar.Value, stamina, change);
    }
}

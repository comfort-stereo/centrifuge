#!/usr/bin/env bash

set -e

GODOT="godot-mono"
SCENE="Scenes/Game.tscn"

cd ..   

${GODOT} --build-solutions -e -q
${GODOT} ${SCENE} 
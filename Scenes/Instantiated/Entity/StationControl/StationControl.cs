using Godot;

public class StationControl : Entity {
    [Install] readonly Area2D Detector;

    int NearbyPlayerCount;

    protected override EntityConfig Config { get; } = new EntityConfig {
        Type = EntityType.Static,
        SyncRate = SyncRate.Constant,
        IsCollidable = false,
        IsLockedToGround = false
    };

    bool IsExcited => NearbyPlayerCount > 0;

    protected override void OnReady() {
        Detector.Connect(Ut.Signal.BodyEntered, this, nameof(OnDetectorEntered));
        Detector.Connect(Ut.Signal.BodyExited, this, nameof(OnDetectorExited));
    }

    protected override void OnAnimate(float delta, Animator animator) {
        if (IsExcited) {
            animator.Animate("float");
        } else {
            animator.AnimateAfter("float", "idle");
        }
    }

    void OnDetectorEntered(KinematicBody2D body) {
        if (body is Avatar avatar) {
            avatar.StationControl.Set(this);
            NearbyPlayerCount++;
        }
    }

    void OnDetectorExited(KinematicBody2D body) {
        if (body is Avatar avatar) {
            avatar.StationControl.Set(null);
            NearbyPlayerCount--;
        }
    }
}
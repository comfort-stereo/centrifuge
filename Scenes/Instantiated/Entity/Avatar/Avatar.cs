using System;
using Godot;

public class Avatar : Entity {
    const float VerticalDashEndSpeedModifier = 0.25f;
    const float DashToWallSpeedModifier = 0.25f;
    
    const int MoveSpeed = 140;
    const int JumpSpeed = 190;
    const int FloatAcceleration = 270;
    
    const int WallJumpSpeed = 180;
    const int WallJumpStaminaCost = 60;
    const int WallJumpPushbackSpeed = 60;
    const int WallJumpPushbackDuration = 400;

    const int WallDragSpeed = 16;
    const int WallDragDecelleration = 8;
    const int WallDragStaminaCostBySecond = 20;
    
    const int MaxStamina = 100;
    
    const int DashDuration = 200;
    const int DashDistance = Constant.BlockSize * 4;

    public readonly Ref<StationControl> StationControl = Ref<StationControl>.Nil;
    
    public Player Player => GetParent() as Player;

    public float Stamina { get; private set; } = MaxStamina; 
    
    public bool IsDashing => DashDirection != Vector.Zero;
    public bool IsWallDragging { get; private set; } 
    public bool IsWallJumping { get; private set; }

    [Install] Camera2D Camera;

    bool CanDash;
    Vector2 DashDirection;
    Side WallJumpSide;

    protected override EntityConfig Config { get; } = new EntityConfig {
        Type = EntityType.Character,
        SyncRate = SyncRate.Constant,
        IsCollidable = true,
        IsLockedToGround = true
    };

    protected override void OnProcess(float delta) {
        if (IsMaster && Player != null) {
            Game.Input.IsEnabled = Game.Control.HasPlayerControl;
            HandleMovement(delta);
            Game.Input.IsEnabled = true;
        }
    }

    protected override void OnAnimate(float delta, Animator animator) {
        if (IsDashing) {
            animator.Animate("air");
        } else if (IsGrounded) {
            var speed = Math.Abs(Velocity.x);
            if (speed < Constant.AssumeZeroSpeed) {
                animator.Animate("idle");
            } else {
                animator.Animate("run", speed / MoveSpeed);
            }
        } else if (IsFacingWall) {
            animator.Animate("wall");
        } else {
            animator.Animate("air");
        }
    }

    protected override void OnSync(float delta, NetworkControl network) {
        network.RPCUSend(this, nameof(SyncAvatar), DashDirection);
    }

    protected override void OnGround(KinematicBody2D body) {
        base.OnGround(body);
        OnGroundTouched();
    }

    protected override void OnFront(KinematicBody2D body) {
        base.OnFront(body);
        OnWallTouched();
    }

    public void Spawn(Vector2 position) {
        Place(position);
    }

    public void Focus() {
        Camera.Current = true;
    }
    
    public void RefreshStamina() {
        Stamina = MaxStamina;
    }
    
    public void RefreshDash() {
        CanDash = true;
    }

    public void ConsumeStamina(float stamina) {
        Stamina = Mathf.Max(0, Stamina - stamina);
    }
    
    void OnGroundTouched() {
        RefreshDash();
        RefreshStamina();
        ClearWallJump();
    }
    
    void OnWallTouched() {
        if (DashDirection.x != 0) {
            StopDash();
        }
    }
    
    void ClearWallJump() {
        IsWallJumping = false;
        WallJumpSide = Side.None;
    }

    void HandleMovement(float delta) {
        HandleFlip(delta);
        if (IsGrounded) {
            OnGroundTouched();
        }
        if (IsFacingWall) {
            OnWallTouched();
        }
        HandleGravity();
        if (HandleDash(delta)) {
            return;
        }
        HandleFlip(delta);
        HandleMove(delta);
        HandleVertical(delta);
        HandleWallDrag(delta);
    }

    void HandleFlip(float delta) {
        if (Game.Input.SnappedMoveAxisX != 0) {
            IsFlipped = Game.Input.SnappedMoveAxisX < 0;
        }
    }

    void HandleMove(float delta) {
        var left = Game.Input.Pressed(Ut.Action.MoveLeft);
        var right = Game.Input.Pressed(Ut.Action.MoveRight);
        
        var x = (left ? -MoveSpeed : 0f) + (right ? MoveSpeed : 0f);
        if (x == 0) {
            x = MoveSpeed * Game.Input.SnappedMoveAxisX;
        } 
        if (x != 0) {
            IsFlipped = x < 0;
        }

        if (IsWallJumping) {
            if (WallJumpSide == Side.Left) {
                x = Mathf.Min(MoveSpeed, Math.Max(WallJumpPushbackSpeed, x));
            } else if (WallJumpSide == Side.Right) {
                x = Mathf.Max(-MoveSpeed, Math.Min(-WallJumpPushbackSpeed, x));
            }
        }

        Velocity = Velocity.SetX(x);
    }

    void HandleVertical(float delta) {
        if (Game.Input.Tapped(Ut.Action.Jump)) {
            Jump();
        }
        
        if (Game.Input.Pressed(Ut.Action.Jump) && IsMovingUp && IsInAir) {
            Velocity += Vector.Up * FloatAcceleration * delta;
        }
    }
    
    bool HandleDash(float delta) {
        if (CanDash && Game.Input.Tapped(Ut.Action.Dash)) {
            Dash(delta);
        }

        return IsDashing;
    }

    bool HandleWallDrag(float delta) {
        IsWallDragging = Stamina > 0 && IsMovingDown && IsFacingWall && Game.Input.Pressed(Ut.Action.WallDrag);
        if (IsWallDragging) {
            Velocity = Velocity.SetY(Mathf.Lerp(Velocity.y, WallDragSpeed, WallDragDecelleration * delta));
            ConsumeStamina(WallDragStaminaCostBySecond * delta);
        }

        return IsWallDragging;
    }

    void HandleGravity() {
        GravityIsEnabled = !IsDashing && !IsWallDragging;
    }

    void Dash(float delta) {
        var direction = Game.Input.MoveVector;
        if (direction == Vector.Zero) {
            return;
        }

        DashDirection = direction; 
        Velocity = DashDirection * (DashDistance / (DashDuration / 1000f));
        CanDash = false;
        
        Game.Task.After(this, DashDuration, StopDash);
    }

    void Jump() {
        if (IsTouchingRoof) {
            return;
        }
        
        if (IsGrounded) {
            Velocity = Velocity.SetY(-JumpSpeed);
            return;
        } 
        
        if (IsFacingWall) {
            if (Stamina < WallJumpStaminaCost) {
                return;
            }
            Velocity = Velocity.SetY(-WallJumpSpeed);
            if (WallJumpSide != Side.None && FacingWallSide == WallJumpSide) {
                ConsumeStamina(WallJumpStaminaCost);
            }
            
            IsWallJumping = true;
            WallJumpSide = FacingWallSide;
            
            Game.Task.After(this, WallJumpPushbackDuration, () => IsWallJumping = false);
        }
    }
    
    void StopDash() {
        DashDirection = Vector.Zero;
        if (IsFacingWall) {
            Velocity *= DashToWallSpeedModifier;
        } else if (IsMovingUp) {
            Velocity = Velocity.SetY(Velocity.y * VerticalDashEndSpeedModifier);
        }
    }
    
    [Slave]
    void SyncAvatar(Vector2 dashDirection) {
        DashDirection = dashDirection;
    }
}
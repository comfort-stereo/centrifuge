using Godot;

public class Platform : TileMap {

    [Export] Vector2 Velocity = Vector.Zero; 

    public override void _PhysicsProcess(float delta) {
        Position = Position + Velocity * delta;
    }
}